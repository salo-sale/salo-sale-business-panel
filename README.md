# SaloSaleAngular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


---

### For Hosting setting with firebase project Salo-Sale

Key Name: _acme-challenge (.salo-sale.com)
Value: MXVz1IgCXsTnXvFWDMVBwwM3UhmP4noDbHils48QbmQ

Key Name: @ (.salo-sale.com)
Value: google-site-verification=S6g1dOY07-Zq4B-yBJrCv-7gIFo5taVe-L7qWjV7hSU


#CODE FOR PARSING JSON
```

    // Dont touch this code!!!
    // const arrayPOS = Object.keys(pointsOfSale['default']['pointsOfSale']).map((key) => pointsOfSale['default']['pointsOfSale'][key]);
    //
    // const array = Object.keys(businessCluster['default']['businessClusters']).map((key) => {
    //
    //   const obj = businessCluster['default']['businessClusters'][key];
    //
    //   obj['contacts'] = isNullOrUndefined(obj['contacts']) ? null : Object.keys(obj['contacts']).map((keyContact) => {
    //
    //     return {
    //       'type': keyContact,
    //       'value': isObject(obj['contacts'][keyContact]) ? null : obj['contacts'][keyContact]
    //     };
    //
    //   }).filter((contact) => contact.value !== null);
    //
    //   obj['tags'] = isNullOrUndefined(obj['tags']) ? null : Object.keys(obj['tags']);
    //
    //   obj['address'] = arrayPOS.find((pos) => {
    //
    //     return pos.businessCluster === obj['id'];
    //
    //   });
    //
    //
    //   if (!isNullOrUndefined(obj['address'])) {
    //
    //     if (!isNullOrUndefined(obj['address']['address'])) {
    //
    //       obj['address'] = obj['address']['address']['street'] + (isNullOrUndefined(obj['address']['address']['office']) ? '' : ' ' + obj['address']['address']['office']);
    //
    //     } else {
    //
    //       obj['address'] = null;
    //
    //     }
    //
    //   } else {
    //
    //     obj['address'] = null;
    //
    //   }
    //
    //   return obj;
    //
    // }).filter((value, index, self) => self.findIndex((val) => val.title === value.title) === index);
    //
    // const functionWithPromise = item => {
    //   return new Promise(async (resolve) => {
    //
    //     if (!isNullOrUndefined(item['address'])) {
    //       await this.appService.authService.apiService.http.get('https://api.opencagedata.com/geocode/v1/json?q=м. Чернівці ' + item['address'] + '&key=e24b9801c37a491aba02b371cc2cac4b').subscribe(async (result) => {
    //
    //         console.log(result);
    //         item['geo'] = await (!isNullOrUndefined(result['results']) && result['results'].length > 0 ? result['results'][0]['geometry'] : null);
    //
    //         resolve(item);
    //
    //       });
    //
    //     } else {
    //
    //       resolve(null);
    //
    //     }
    //
    //   });
    // };
    //
    // const anAsyncFunction = async item => {
    //   return functionWithPromise(item);
    // };
    //
    // const getData = async () => {
    //   return Promise.all(array.map(item => anAsyncFunction(item)));
    // };
    //
    // getData().then(data => {
    //   this.sendArray(data);
    // });
    // Finihs dont touch code!!!
```
