import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import {AdminPanelService} from './shared/services/admin-panel.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin-panel.component.html',
  styleUrls: [
    './admin-panel.component.scss',
  ]
})
export class AdminPanelComponent implements OnInit {

  constructor(
    public adminService: AdminPanelService
  ) {

  }

  ngOnInit() {

    const body = $('body');


    body.on('click', '#btn-to-top', (e) => {

      $('.page-wrapper').scrollTop(0);

    });

    // Scroll to top button appear
    $('.page-wrapper').on('scroll', function() {
      const scrollDistance = $(this).scrollTop();
      if (scrollDistance > 100) {
        $('#btn-to-top').fadeIn();
      } else {
        $('#btn-to-top').fadeOut();
      }
    });

  }

}
