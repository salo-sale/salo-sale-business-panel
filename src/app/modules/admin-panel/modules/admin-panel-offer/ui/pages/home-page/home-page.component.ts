import {AfterViewChecked, Component, forwardRef, Inject, Input, OnDestroy, OnInit} from '@angular/core';
import {AdminPanelService} from '../../../../../shared/services/admin-panel.service';
import {OfferService} from '../../../shared/services/offer.service';
import {OfferModel} from '../../../shared/models/offer.model';
import {environment} from '../../../../../../../../environments/environment';
import {CompanyStatusEnum} from '../../../../../../../shared/enums/status/company.status.enum';
import {OfferStatusEnum} from '../../../../../../../shared/enums/status/offer.status.enum';
import {isNullOrUndefined} from 'util';
import {DeviceDetectorService} from 'ngx-device-detector';

@Component({
  selector: 'app-admin-panel-offer-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit, OnDestroy {
  id = 'offers';
  pageId = 'app-admin-offers-home';

  public initialized = false;
  public initHideBody = true;
  public collapseContent = true;
  public loading = true;

  isDesktop = false;

  public adminService: AdminPanelService;
  public offerService: OfferService;

  @Input()
  companyId: number = null;
  accessForUse: string[];
  accessForUser: string[];
  types: string[];

  constructor(
    @Inject(forwardRef(() => AdminPanelService)) adminService,
    @Inject(forwardRef(() => OfferService)) offerService,
    private deviceService: DeviceDetectorService
  ) {


    this.isDesktop = deviceService.isDesktop();
    this.offerService = offerService;
    this.adminService = adminService;
    this.accessForUse = environment.accessForUse;
    this.accessForUser = environment.accessForUser;
    this.types = environment.typeOffer;

  }


  clickOnRefreshBtn() {

    this.doFiltering();

  }

  clickOnCollapseBtn() {

    this.collapseContent = !this.collapseContent;

    console.log(this.filter.total);

    if (this.filter.total === 0 && this.collapseContent) {

      this.doFiltering();

    }

  }

  private init() {

    if (!this.initialized) {

      this.initialized = true;


    }

  }

  loader(run: boolean) {
    this.loading = run;
  }


  get filter () {

    return this.offerService.filter;

  }

  clearSearch() {

    this.offerService.filter.search = '';
    this.doFiltering();

  }

  get offers(): OfferModel[] {

    return this.offerService.offers;

  }

  getStatusName(status) {

    return OfferStatusEnum[status];

  }

  getTypeName(type) {

    return environment.typeOffer[type];

  }

  doFiltering(initPageList: boolean = true) {

    this.loader(true);

    if (initPageList) {

      this.offerService.filter.resetPage();

    }

    this.offerService.getOffers(true, this.companyId).then(() => {

      if (initPageList) {

        this.offerService.filter.initPageList();

      }

      this.loader(false);

    });

  }

  ngOnInit() {

    if (isNullOrUndefined(this.companyId)) {
      this.adminService.titlePage = 'Пропозиції';
      this.initHideBody = false;
    } else {
      this.initHideBody = true;
    }

    this.collapseContent = !this.initHideBody;

    this.init();

    if (this.initHideBody) {

      this.loader(false);

    } else {

      this.doFiltering();

    }

  }

  selectOffer(offer: OfferModel) {
    console.log(offer);
    this.offerService.document = offer;
  }

  ngOnDestroy(): void {
    this.offerService.init();
  }
}
