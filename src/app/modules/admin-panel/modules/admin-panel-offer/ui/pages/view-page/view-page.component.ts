import {Component, ElementRef, forwardRef, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AdminPanelService} from '../../../../../shared/services/admin-panel.service';
import {ActivatedRoute} from '@angular/router';
import {OfferService} from '../../../shared/services/offer.service';
import {OfferModel} from '../../../shared/models/offer.model';
import {ImageCroppedEvent} from 'ngx-image-cropper';
import {ConvertTool} from '../../../../../../../shared/tools/convert.tool';
import {AppService} from '../../../../../../../shared/services/app.service';
import {environment} from '../../../../../../../../environments/environment';
import {OfferLanguageModel} from '../../../shared/models/offer-language.model';
import {OfferStatusEnum} from '../../../../../../../shared/enums/status/offer.status.enum';
import {isNullOrUndefined} from "util";

@Component({
  selector: 'app-view-page',
  templateUrl: './view-page.component.html',
  styleUrls: ['./view-page.component.scss']
})
export class ViewPageComponent implements OnInit, OnDestroy {

  id = 'offer-view';
  pageId = 'app-admin-offer-view-view';

  loading = true;
  public paramID: string;

  public accessForUse: string[];
  public accessForUser: string[];
  public aspectRation: number = 16 / 9;

  @ViewChild('bannerInput')
  bannerInputVar: ElementRef;

  constructor(
    @Inject(forwardRef(() => AdminPanelService)) adminService,
    private activatedRoute: ActivatedRoute,
    public offerService: OfferService,
    public appService: AppService,
  ) {

    this.accessForUse = environment.accessForUse;
    this.accessForUser = environment.accessForUser;
    adminService.titlePage = 'Перегляд пропозиції';

  }

  get offer(): OfferModel {

    return this.offerService.document;

  }

  //
  // Start: Image cropper
  //

  imageChangedEvent: any = null;
  croppedImage: any = null;

  //
  // Finish: Image cropper
  //
  selectedLanguage: OfferLanguageModel;

  image(languageCode: string): string {

    // https://salo-sale.com/uploads/companies/{{ offer.companyId }}/offers/{{ offer.id }}/images/1.jpg
    return environment.host + 'uploads/banners/' + this.offer.id + '/' + languageCode + '.jpg';


  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    console.log(event.file);
    console.log(event.base64);
    console.log(ConvertTool.b64toBlob(event.base64));
  }
  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }

  ngOnDestroy(): void {

    this.offerService.document = null;

  }

  ngOnInit() {

    this.activatedRoute.parent.firstChild.params.subscribe(async (params) => {

      this.paramID = params.id;
      this.offerService.getOffer(this.paramID).then(() => {

        this.loader(false);

      });

    });

  }

  clickOnRefreshBtn() {

    if (isNullOrUndefined(this.paramID)) {
      return;
    }

    this.loader(true);
    this.offerService.getOffer(this.paramID).then(() => {

      this.loader(false);

    });

  }

  loader(run: boolean) {
    this.loading = run;
  }

  saveUploadImage(languageCode: string) {

    console.log(this.croppedImage);
    console.log(this.imageChangedEvent);

    this.offerService.saveImage(this.offer, ConvertTool.blobToFile(ConvertTool.b64toBlob(this.croppedImage), '1.jpg'), languageCode).then(() => {

      this.clearNewImg();

    });
  }

  languageName(code) {

    return this.appService.languages.find((language) => language.code === code).name;

  }


  getStatusName(status) {

    return OfferStatusEnum[status];

  }

  getTypeName(type: number) {
    return environment.typeOffer[type];
  }

  clearNewImg() {

    this.croppedImage = null;
    this.imageChangedEvent = null;
    this.bannerInputVar.nativeElement.value = '';

  }
}
