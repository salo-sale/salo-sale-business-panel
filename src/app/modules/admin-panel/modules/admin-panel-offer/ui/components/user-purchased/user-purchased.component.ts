import {Component, forwardRef, Inject, Input, OnInit} from '@angular/core';
import {OfferService} from '../../../shared/services/offer.service';
import {OfferModel} from '../../../shared/models/offer.model';
import {CodeModel} from '../../../shared/models/code.model';
import {isNullOrUndefined} from 'util';
import {FormBuilder, FormGroup} from '@angular/forms';
import { CodeStatusEnum } from 'src/app/shared/enums/status/code.status';
import {DeviceDetectorService} from 'ngx-device-detector';
import {UserPurchasedModel} from '../../../shared/models/user-purchased.model';
import {UserPurchasedStatusEnum} from '../../../../../../../shared/enums/status/user-purchased.status';

@Component({
  selector: 'app-admin-panel-offer-user-purchased-component',
  templateUrl: './user-purchased.component.html',
  styleUrls: ['./user-purchased.component.scss']
})
export class UserPurchasedComponent implements OnInit {
  id = 'offer-user-purchaseds';
  pageId = 'app-admin-offer-user-purchaseds-home';

  public initialized = false;
  public initHideBody = true;
  public collapseContent = true;
  public loading = true;
  public selectedDocument: UserPurchasedModel;

  @Input()
  public offerId = null;

  @Input()
  public companyId = null;

  isDesktop = false;
  public offerService: OfferService;
  addCodeFormGroup: FormGroup;

  constructor(
    @Inject(forwardRef(() => OfferService)) offerService,
    private deviceService: DeviceDetectorService
  ) {


    this.isDesktop = deviceService.isDesktop();
    this.offerService = offerService;

  }

  get documents(): UserPurchasedModel[] {

    return this.offerService.offerUserPurchased;

  }

  getStatusName(status: number) {
    return UserPurchasedStatusEnum[status];
  }

  doFiltering(initPageList: boolean = true) {

    this.loader(true);

    if (initPageList) {

      this.offerService.filter.resetPage();

    }

    this.offerService.getUserPurchasedList(true).then(() => {

      if (initPageList) {

        this.offerService.filterCode.initPageList();

      }

      this.loader(false);
    });

  }

  ngOnInit() {

    this.filter.companyId = this.companyId;
    this.filter.offerId = this.offerId;
    this.initHideBody = this.offerId != null;
    this.collapseContent = !this.initHideBody;

    this.init();

    if (this.initHideBody) {

      this.loader(false);

    } else {

      this.doFiltering();

    }

  }

  private init() {

    if (!this.initialized) {

      this.initialized = true;


    }

  }

  loader(run: boolean) {
    this.loading = run;
  }

  changeStatus(status: number, model: UserPurchasedModel) {

    if (confirm('Впевнений що хочеш змінити статус покупки з куплено на використано?')) {

      model.status = status;
      this.offerService.changeStatusUserPurchased(model).then(async () => {

        await this.offerService.getCodes();

      });
    }

  }

  clickOnRefreshBtn() {

    this.doFiltering();

  }

  clickOnCollapseBtn() {

    this.collapseContent = !this.collapseContent;

    console.log(this.filter.total);

    if (this.filter.total === 0 && this.collapseContent) {

      this.doFiltering();

    }

  }

  initFormGroup() {
    // TODO

    this.addCodeFormGroup = new FormBuilder().group({
      status: [
        1,
      ],
      code: [
        ''
      ],
      offerId: [
        this.offerService.document.id
      ]
    });

  }

  get filter() {

    return this.offerService.filterUserPurchased;

  }

  save() {

    if (this.addCodeFormGroup.valid) {

      this.offerService.saveUserPurchased(new UserPurchasedModel().deserialize(this.addCodeFormGroup.value)).then(() => {

        this.doFiltering();

      });

    }

  }

}
