import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPurchasedComponent } from './user-purchased.component';

describe('CodeComponent', () => {
  let component: UserPurchasedComponent;
  let fixture: ComponentFixture<UserPurchasedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPurchasedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPurchasedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
