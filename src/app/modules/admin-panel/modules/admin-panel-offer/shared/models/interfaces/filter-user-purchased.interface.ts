export interface FilterUserPurchasedInterface {

  page: number;
  count: number;
  orderBy: string;
  sort: string;

  search: string;
  accessForUser: number;
  accessForUse: number;
  type: number;
  companyId: number;
  offerId: number;
  limit: number;

}
