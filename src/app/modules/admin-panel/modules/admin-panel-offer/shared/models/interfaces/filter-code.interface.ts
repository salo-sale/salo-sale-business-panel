export interface FilterCodeInterface {

  page: number;
  count: number;
  orderBy: string;
  sort: string;

  search: string;
  accessForUser: number;
  accessForUse: number;
  type: number;
  companyId: number;
  limit: number;

}
