export interface ValidityOfferInterface {

  id: number;
  offerId: number;
  openTime: number;
  closeTime: number;
  weekday: number;
  canUse: number;
  isRange: number;
  isFrom: number;
  validityDate: {year: number, month: number, day: number} & Date | Date;

}
