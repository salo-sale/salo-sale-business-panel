import {ValidityOfferInterface} from './interfaces/validity-offer.interface';
import {OfferInterface} from './interfaces/offer.interface';
import {DeserializableInterface} from '../../../../../../shared/models/interfaces/deserializable.interface';
import {SerializableInterface} from '../../../../../../shared/models/interfaces/serializable.interface';
import {isNullOrUndefined} from "util";

export class ValidityOfferModel implements ValidityOfferInterface, DeserializableInterface<ValidityOfferInterface>, SerializableInterface<ValidityOfferInterface> {

  canUse: number;
  closeTime: number;
  validityDate: Date;
  id: number;
  isFrom: number;
  isRange: number;
  offerId: number;
  openTime: number;
  weekday: number;

  _isNew: boolean;

  constructor() {
    this.initModel();
  }


  deserialize(input: ValidityOfferInterface, isNew = false): this {

    console.log(input);
    this._isNew = isNew;
    const object = Object.assign(this, input);

    if (isNullOrUndefined(object.isFrom)) {
      object.isFrom = 0;
    }

    if (isNullOrUndefined(object.isRange)) {
      object.isRange = 0;
    }

    if (isNullOrUndefined(object.canUse)) {
      object.canUse = 1;
    }

    if (object.canUse) {
      object.canUse = 1;
    }


    if (!isNullOrUndefined(object.validityDate)) {

      if (typeof object.validityDate === 'string') {

        object.validityDate = new Date(object.validityDate);

      } else {

        console.log(object.validityDate['month']);
        console.log(typeof object.validityDate === 'string');
        object.validityDate = new Date((object.validityDate['year'] + '-' + object.validityDate['month'] + '-' + object.validityDate['day']));
      }

    }
    return object;

  }

  serialize(): ValidityOfferInterface {

    const object = Object.assign({} as ValidityOfferInterface, this);
    console.log(object);
    delete object._isNew;

    if (isNullOrUndefined(object.isFrom)) {
      object.isFrom = 0;
    }

    if (isNullOrUndefined(object.isRange)) {
      object.isRange = 0;
    }

    if (isNullOrUndefined(object.canUse)) {
      object.canUse = 1;
    }

    if (object.canUse) {
      object.canUse = 1;
    }

    if (!isNullOrUndefined(object.validityDate)) {

      // object.validityDate = {
      //   year: object.validityDate.getFullYear(),
      //   month: object.validityDate.getMonth() + 1,
      //   day: object.validityDate.getDate()
      // } as Date | {year: number, month: number, day: number} & Date;

    }

    return object;

  }

  public initModel() {

    this._isNew = true;

  }

}
