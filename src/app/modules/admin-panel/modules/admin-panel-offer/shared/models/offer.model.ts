import {OfferInterface} from './interfaces/offer.interface';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {DeserializableInterface} from '../../../../../../shared/models/interfaces/deserializable.interface';
import {SerializableInterface} from '../../../../../../shared/models/interfaces/serializable.interface';
import {ValidityOfferInterface} from './interfaces/validity-offer.interface';
import {OfferLanguageModel} from './offer-language.model';
import {CompanyModel} from '../../../admin-panel-company/shared/models/company.model';
import {environment} from '../../../../../../../environments/environment';
import {ValidityOfferModel} from './validity-offer.model';
import {isNullOrUndefined} from 'util';
import {LanguageModel} from '../../../../../../shared/models/language.model';
import {LanguageInterface} from '../../../../../../shared/models/interfaces/language.interface';
import {OfferLocalityInterface} from './interfaces/offer-locality.interface';
import {ImageInterface} from '../../../../shared/models/interfaces/image.interface';


export class OfferModel implements OfferInterface, DeserializableInterface<OfferInterface>, SerializableInterface<OfferInterface> {

  /**
   *
   * Declaration variables
   *
   */

  private formBuilder: FormGroup;

  id: number;
  price: number;
  limit: number;
  numberOfViews: number;
  amountOfUses: number;
  validityOffers: ValidityOfferModel[];
  income: number;
  offerLocalityList: OfferLocalityInterface[];
  accessForUser: number;
  type: number;
  canBuy: number;
  status: number;
  companyId: number;
  ikointForTake: number;
  accessForUse: number;
  languages: OfferLanguageModel[];
  updatedAt: string;
  company: CompanyModel;
  tags: number[];

  _isNew: boolean;
  pointsOfSale: string[];
  bannerUrl: ImageInterface[];
  logoUrl: ImageInterface;

  constructor() {

    this._isNew = true;
    this.initModel();
    this.initForm();

  }

  deserialize(input: OfferInterface): this {

    this._isNew = false;

    const object = Object.assign(this, input);

    // TODO delete and create component
    if (!isNullOrUndefined(object.validityOffers) && object.validityOffers.length > 0) {

      object.validityOffers = object.validityOffers.map((validity) => {

        return new ValidityOfferModel().deserialize(validity);

      });

    }

    if (!isNullOrUndefined(object.languages) && object.languages.length > 0) {

      object.languages = object.languages.map((language) => {

        return new OfferLanguageModel().deserialize(language);

      });

      object.tags = object.languages[0].tags.map((tag) => tag.tagId);

    }

    object.company = new CompanyModel().deserialize(object.company);

    return object;

  }

  serialize(): OfferInterface {

    return {

      id: this.id,
      price: this.price,
      limit: this.limit,
      // validityOffers: this.validityOffers.map((v) => v.serialize()),
      income: this.income,
      accessForUser: this.accessForUser,
      type: this.type,
      canBuy: this.canBuy,
      numberOfViews: this.numberOfViews,
      amountOfUses: this.amountOfUses,
      status: this.status,
      companyId: isNullOrUndefined(this.companyId) ? isNullOrUndefined(this.company) ? null : this.company.id : this.companyId,
      ikointForTake: this.ikointForTake,
      accessForUse: this.accessForUse,
      languages: this.languages.map((l) => l.serialize()),
      tags: this.tags,
      offerLocalityList: this.offerLocalityList,

    };

  }

  get formGroup(): FormGroup {

    return this.formBuilder;

  }

  get validForm(): boolean {

    if (this.formGroup.valid) {

      // this.deserialize(this);
      //
      // console.log(this);

      const value = this.formGroup.value;

      this.price = value.price;
      this.company = value.company;
      this.status = value.status;
      this.limit = value.limit;
      this.income = value.income;
      this.tags = value.tags;
      this.canBuy = value.canBuy ? 1 : 0;
      this.accessForUser = value.accessForUser;
      this.type = value.type;
      this.ikointForTake = value.ikointForTake;
      this.accessForUse = value.accessForUse;
      this.languages = value.languages.map((l: LanguageInterface) => new LanguageModel().deserialize(l));
      // this.validityOffers = value.validityOffers.map((v: ValidityOfferInterface) => new ValidityOfferModel().deserialize(v));

      return true;

    } else {

      return false;

    }

  }

  public initForm(): void {

    let languages = [
      // new FormGroup({
      //
      //   languageCode: new FormControl('en', [Validators.required]),
      //   title: new FormControl('Offer', [Validators.required]),
      //   description: new FormControl('Offer', [Validators.required]),
      //
      // }),
      // new FormGroup({
      //
      //   languageCode: new FormControl('ru', [Validators.required]),
      //   title: new FormControl('Скидка', [Validators.required]),
      //   description: new FormControl('Скидка', [Validators.required]),
      //
      // }),
      new FormGroup({

        languageCode: new FormControl('uk', [Validators.required]),
        descriptionRequirements: new FormControl(null),
        title: new FormControl(null, [Validators.required]),
        description: new FormControl(null, [Validators.required]),

      })
    ];

    // let validityOffers = [];

    if (!this._isNew) {

      languages = [...this.languages.map((language) => {

        return new FormGroup({

          id: new FormControl(language.id, [Validators.required]),
          descriptionRequirements: new FormControl(language.descriptionRequirements),
          languageCode: new FormControl(language.languageCode, [Validators.required]),
          title: new FormControl(language.title, [Validators.required]),
          description: new FormControl(language.description, [Validators.required]),

        });

      })];

      // validityOffers = [...this.validityOffers.map((validity) => {
      //
      //   return new FormGroup({
      //
      //     id: new FormControl(validity.id, [Validators.required]),
      //     openTime: new FormControl(validity.openTime),
      //     closeTime: new FormControl(validity.closeTime),
      //     weekday: new FormControl(validity.weekday),
      //     day: new FormControl(validity.day),
      //     month: new FormControl(validity.month),
      //     year: new FormControl(validity.year),
      //     canUse: new FormControl(validity.canUse),
      //     isRange: new FormControl(validity.isRange),
      //     isFrom: new FormControl(validity.isFrom),
      //     isTo: new FormControl(validity.isTo),
      //
      //   });
      //
      // })];

    }

    this.formBuilder = new FormBuilder().group({
      price: [
        this.price,
        [
          Validators.required
        ]
      ],
      limit: [
        this.limit,
      ],
      income: [
        this.income,
      ],
      accessForUser: [
        this.accessForUser,
      ],
      type: [
        this.type,
      ],
      ikointForTake: [
        this.ikointForTake,
      ],
      accessForUse: [
        this.accessForUse,
      ],
      tags: [
        this.tags,
      ],
      canBuy: [
        this.canBuy,
      ],
      status: [
        this.status,
      ],
      company: [
        this.company,
        this._isNew ? [Validators.required] : []
      ],
      // tags: new FormArray([], [Validators.required]),
      // validityOffers: new FormArray(validityOffers),
      languages: new FormArray(languages, [Validators.required]),
    });

  }

  initModel() {

    this.offerLocalityList = [
      {
        localityId: 1,
        without: 0
      },
      {
        localityId: 2,
        without: 0
      },
      {
        localityId: 3,
        without: 0
      },
    ];
    this.price = environment.defaultDate.offer.price;
    this.limit = environment.defaultDate.offer.limit;
    this.ikointForTake = environment.defaultDate.offer.ikointForTake;
    this.type = environment.defaultDate.offer.type;
    this.accessForUse = environment.defaultDate.offer.accessForUse;
    this.accessForUser = environment.defaultDate.offer.accessForUser;
    this.status = 1;
    this.canBuy = 0;
    this.companyId = null;

  }

  get formLanguages(): FormArray {
    return this.formGroup.get('languages') as FormArray;
  }

  addNewLanguageToForm() {

    this.formLanguages.push(new FormGroup({

      languageCode: new FormControl('uk', [Validators.required]),
      title: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),

    }));

  }

  deleteFormLanguage(index: number) {
    this.formLanguages.controls.splice(index, 1);
  }
}
