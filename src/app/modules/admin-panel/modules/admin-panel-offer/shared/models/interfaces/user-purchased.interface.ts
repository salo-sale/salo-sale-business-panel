import {UserInterface} from '../../../../../../../shared/models/interfaces/user.interface';

export interface UserPurchasedInterface {

  id: number;
  code: string;
  offerId: number;
  status: number;
  createdAt: string;
  updatedAt: string;
  user: UserInterface;
  userId: string;

}
