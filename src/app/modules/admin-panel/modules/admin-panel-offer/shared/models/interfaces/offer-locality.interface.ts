export interface OfferLocalityInterface {

  id?: number;
  offerId?: number;
  localityId: number;
  without: number;

}
