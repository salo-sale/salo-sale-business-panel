import {DeserializableInterface} from '../../../../../../shared/models/interfaces/deserializable.interface';
import {SerializableInterface} from '../../../../../../shared/models/interfaces/serializable.interface';
import {LinkInterface} from './interfaces/link.interface';

export class LinkModel implements LinkInterface, DeserializableInterface<LinkInterface>, SerializableInterface<LinkInterface> {

  link: string;
  createdAt: string;
  offerId: number;
  status: number;
  updatedAt: string;


  _isNew: boolean;

  constructor() {
    this.initModel();
  }


  deserialize(input: LinkInterface): this {

    this._isNew = false;

    return Object.assign(this, input);

  }

  serialize(): LinkInterface {

    const object = Object.assign({} as LinkInterface, this);
    delete object._isNew;

    return object;

  }

  public initModel() {

    this._isNew = true;

  }

}
