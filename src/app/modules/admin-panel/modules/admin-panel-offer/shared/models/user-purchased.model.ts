import {OfferLanguageInterface} from './interfaces/offer-language.interface';
import {DeserializableInterface} from '../../../../../../shared/models/interfaces/deserializable.interface';
import {SerializableInterface} from '../../../../../../shared/models/interfaces/serializable.interface';
import {TagModel} from '../../../../shared/models/tag.model';
import {UserPurchasedInterface} from './interfaces/user-purchased.interface';
import {UserModel} from '../../../../../../shared/models/user.model';

export class UserPurchasedModel implements
  UserPurchasedInterface,
  DeserializableInterface<UserPurchasedInterface>,
  SerializableInterface<UserPurchasedInterface> {


  code: string;
  createdAt: string;
  id: number;
  offerId: number;
  status: number;
  updatedAt: string;
  user: UserModel;
  userId: string;

  _isNew: boolean;

  constructor() {

    this._isNew = true;

  }

  deserialize(input: UserPurchasedInterface): this {

    this._isNew = false;
    const object = Object.assign(this, input);
    object.user = new UserModel().deserialize(object.user);
    return object;

  }

  serialize(): UserPurchasedInterface {

    const object = Object.assign({} as UserPurchasedInterface, this);
    delete object._isNew;
    delete object.user;
    delete object.createdAt;
    delete object.updatedAt;

    return object;

  }


}
