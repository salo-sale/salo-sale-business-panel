import { Injectable } from '@angular/core';
import {AppService} from '../../../../../../shared/services/app.service';
import {RequestConfigurationModel} from '../../../../../../shared/models/request-configuration.model';
import {OfferModel} from '../models/offer.model';
import {OfferInterface} from '../models/interfaces/offer.interface';
import {FilterOfferModel} from '../models/filter-offer.model';
import {isNull, isNullOrUndefined} from 'util';
import {NotificationStyle} from '../../../../shared/modules/notification/shared/enums/notification-style.enum';
import {NotificationService} from '../../../../shared/modules/notification/shared/services/notification.service';
import {CodeModel} from '../models/code.model';
import {LinkModel} from '../models/link.model';
import {LinkInterface} from '../models/interfaces/link.interface';
import {CodeInterface} from '../models/interfaces/code.interface';
import {FilterCodeModel} from '../models/filter-code.model';
import {FilterLinkModel} from '../models/filter-link.model';
import {PointOfSaleModel} from '../../../admin-panel-point-of-sale/shared/models/point-of-sale.model';
import {OfferLocalityInterface} from '../models/interfaces/offer-locality.interface';
import {RecommendedOfferModel} from '../../../admin-panel-advertising-banner/shared/models/recommended-offer.model';
import {ValidityOfferModel} from '../models/validity-offer.model';
import {ValidityOfferInterface} from '../models/interfaces/validity-offer.interface';
import {FilterValidityModel} from '../models/filter-validity.model';
import {UserPurchasedModel} from '../models/user-purchased.model';
import {FilterUserPurchasedModel} from '../models/filter-user-purchased.model';
import {UserPurchasedInterface} from '../models/interfaces/user-purchased.interface';

@Injectable({
  providedIn: 'root'
})
export class OfferService {

  public filter: FilterOfferModel;
  public filterCode: FilterCodeModel;
  public offers: OfferModel[];
  public filterUserPurchased: FilterUserPurchasedModel;
  private initialize: boolean;
  document: OfferModel;
  public companyId: number;
  public offerCodes: CodeModel[];
  public offerLinks: LinkModel[];
  public validityList: ValidityOfferModel[];
  public offerUserPurchased: UserPurchasedModel[];
  filterLink: FilterLinkModel;
  filterValidity: FilterValidityModel;

  constructor(
    public appService: AppService,
    public notificationService: NotificationService
  ) {

    this.init();

  }

  /**
   *
   * @param reset
   * @param companyId
   */
  async getOffers(reset: boolean = false, companyId: number = null): Promise<OfferModel[]> {

    return new Promise<OfferModel[]>(async (resolve, reject) => {

      if (this.offers.length === 0 || reset) {

        const filter = this.filter.serialize();

        delete filter['total'];
        delete filter['pageList'];

        if (!isNull(companyId)) {

          filter.companyId = companyId;

        }

        const requestConfiguration = new RequestConfigurationModel().deserialize({
          path: 'business',
          url: 'offers',
          params: {
            options: filter,
          }
        });

        await this.appService.authService.apiService.request(requestConfiguration).subscribe((data: {
          models: OfferInterface[],
          total: number
        }) => {

          this.offers = [];

          this.offers = data.models.map((offer) => new OfferModel().deserialize(offer));
          this.filter.total = data.total;

          resolve(this.offers);

        });

      } else {

        resolve(this.offers);

      }

    });

  }

  /**
   *
   * @param reset
   */
  async getValidityList(reset: boolean = false): Promise<ValidityOfferModel[]> {

    return new Promise<ValidityOfferModel[]>(async (resolve, reject) => {

      if (this.validityList.length === 0 || reset) {

        this.filterValidity.offerId = this.document.id;

        const filter = this.filterValidity.serialize();

        delete filter['total'];
        delete filter['pageList'];

        const requestConfiguration = new RequestConfigurationModel().deserialize({
          path: 'business',
          url: 'offer-validity',
          params: {
            options: filter,
          }
        });

        await this.appService.authService.apiService.request(requestConfiguration).subscribe((data: {
          models: ValidityOfferInterface[],
          total: number
        }) => {

          this.validityList = [];

          this.validityList = data.models.map((offer) => new ValidityOfferModel().deserialize(offer));
          this.filterValidity.total = data.total;
          console.log(this.validityList);

          resolve(this.validityList);

        });

      } else {

        resolve(this.validityList);

      }

    });

  }

  /**
   *
   * @param reset
   */
  async getUserPurchasedList(reset: boolean = false): Promise<UserPurchasedModel[]> {

    return new Promise<UserPurchasedModel[]>(async (resolve, reject) => {

      if (this.offerUserPurchased.length === 0 || reset) {

        const filter = this.filterUserPurchased.serialize();

        delete filter['total'];
        delete filter['pageList'];

        const requestConfiguration = new RequestConfigurationModel().deserialize({
          path: 'business',
          url: 'user-purchaseds',
          params: {
            options: filter,
          }
        });

        await this.appService.authService.apiService.request(requestConfiguration).subscribe((data: {
          models: UserPurchasedInterface[],
          total: number
        }) => {

          this.offerUserPurchased = [];

          this.offerUserPurchased = data.models.map((offer) => new UserPurchasedModel().deserialize(offer));
          this.filterUserPurchased.total = data.total;

          resolve(this.offerUserPurchased);

        });

      } else {

        resolve(this.offerUserPurchased);

      }

    });

  }

  /**
   *
   * @param reset
   */
  async getCodes(reset: boolean = false): Promise<CodeModel[]> {

    return new Promise<CodeModel[]>(async (resolve, reject) => {

      if (this.offerCodes.length === 0 || reset) {

        const filter = this.filterCode.serialize();

        delete filter['total'];
        delete filter['pageList'];

        const requestConfiguration = new RequestConfigurationModel().deserialize({
          path: 'business',
          url: 'offer-codes',
          subUrl: '/' + this.document.id.toString(),
          params: {
            options: filter,
          }
        });

        await this.appService.authService.apiService.request(requestConfiguration).subscribe((data: {
          models: CodeInterface[],
          total: number
        }) => {

          this.offerCodes = [];

          this.offerCodes = data.models.map((offer) => new CodeModel().deserialize(offer));
          this.filterCode.total = data.total;

          resolve(this.offerCodes);

        });

      } else {

        resolve(this.offerCodes);

      }

    });

  }

  /**
   *
   * @param reset
   */
  async getLinks(reset: boolean = false): Promise<LinkModel[]> {

    return new Promise<LinkModel[]>(async (resolve, reject) => {

      if (this.offerLinks.length === 0 || reset) {

        const filter = this.filterLink.serialize();

        delete filter['total'];
        delete filter['pageList'];

        const requestConfiguration = new RequestConfigurationModel().deserialize({
          path: 'business',
          url: 'offer',
          subUrl: this.document.id + '/links',
          params: {
            options: filter,
          }
        });

        await this.appService.authService.apiService.request(requestConfiguration).subscribe((data: {
          models: LinkInterface[],
          total: number
        }) => {

          this.offerLinks = [];

          this.offerLinks = data.models.map((offer) => new LinkModel().deserialize(offer));
          this.filterLink.total = data.total;

          resolve(this.offerLinks);

        });

      } else {

        resolve(this.offerLinks);

      }

    });

  }

  init() {

    this.filter = new FilterOfferModel();
    this.filter.companyId = this.appService.authService.user.selectedCompany.id;
    this.filterLink = new FilterLinkModel();
    this.filterCode = new FilterCodeModel();
    this.filterValidity = new FilterValidityModel();
    this.filterUserPurchased = new FilterUserPurchasedModel();
    this.offers = [];
    this.offerLinks = [];
    this.offerCodes = [];
    this.offerUserPurchased = [];
    this.validityList = [];
    this.document = null;

    if (!this.initialize) {

      // this.offers = LocalStorageTool.getOffers();
      //
      // this.filter = LocalStorageTool.getFilterOffer();
      this.filter.initPageList();

      this.initialize = true;

    }

  }

  /**
   *
   * @param model
   */
  async saveUserPurchased(model: UserPurchasedModel): Promise<number> {

    return new Promise(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'business',
        url: 'user-purchaseds',
        subUrl: model._isNew ? '' : '/' + model.id.toString(),
        params: {
          type: model._isNew ? 'post' : 'put',
          body: model.serialize()
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((id: number) => {

        console.log(id);

        this.notificationService.addToStream({
          title: 'Покупки',
          body: 'Успішно записано.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        this.document = null;

        resolve(id);

      }, error => {

        console.log(error);


        this.notificationService.addToStream({
          title: 'Покупки',
          body: error.error,
          show: true,
          style: NotificationStyle.ERROR
        });

        resolve(0);

      });

    });

  }

  /**
   *
   * @param model
   */
  async saveOffer(model: OfferModel): Promise<number> {

    return new Promise(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'business',
        url: model._isNew ? 'offers' : 'offer',
        subUrl: model._isNew ? '' : model.id.toString(),
        params: {
          type: model._isNew ? 'post' : 'put',
          body: model.serialize()
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((id: number) => {

        console.log(id);

        this.notificationService.addToStream({
          title: 'Пропозиція',
          body: 'Успішно записано.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        this.document = null;

        resolve(id);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Пропозиція',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(0);

      });

    });

  }

  /**
   *
   * @param model
   * @param image
   * @param languageCode
   */
  async saveImage(model: OfferModel, image: File, languageCode: string = 'uk'): Promise<boolean | string> {

    const formData = new FormData();

    formData.append('offerId', model.id.toString());
    formData.append('companyId', model.companyId.toString());
    formData.append('languageCode', languageCode);
    formData.append('imageFile', image, '1');

    return new Promise(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'business',
        url: 'offer',
        subUrl: model.id.toString() + '/image',
        params: {
          type: 'post',
          body: formData
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((id: number) => {

        console.log(id);

        this.notificationService.addToStream({
          title: 'Картинка пропозиції',
          body: 'Успішно записано.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve(true);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Картинка пропозиції',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(false);

      });

    });

  }

  /**
   *
   * @param id
   */
  async getOffer(id: string): Promise<OfferModel> {

    return new Promise(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'business',
        url: 'offers',
        subUrl: '/' + id.toString()
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((offer: OfferInterface) => {

        // console.log(pffer);
        this.document = new OfferModel().deserialize(offer);

        resolve(this.document);

      });

    });

  }

  /**
   *
   * @param offerLocality
   * @param remove
   */
  async toggleOfferLocality(offerLocality: OfferLocalityInterface, remove = false): Promise<number> {

    console.log(offerLocality, remove);

    return new Promise(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'business',
        url: 'offer',
        subUrl: offerLocality.offerId.toString() + '/toggle-locality/' + (remove ? offerLocality.id.toString() : '0'),
        params: {
          type: remove ? 'delete' : 'post',
          body: remove ? null : offerLocality
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((id: number) => {

        console.log(id);

        this.notificationService.addToStream({
          title: 'Локалізація',
          body: 'Успішно записано.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve(id);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Точка продажу',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(0);

      });

    });

  }

  async changeStatusCode(code: CodeModel): Promise<any> {

    return new Promise<any>(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'business',
        url: 'offer-codes',
        subUrl: '/' + this.document.id.toString() + '/code',
        params: {
          type: 'put',
          body: code.serialize()
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((result) => {

        console.log(result);

        this.notificationService.addToStream({
          title: 'Код пропозиції: ' + code.code,
          body: 'Успішно записано.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve(result);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Пропозиція',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(0);

      });

    });

  }

  async changeStatusUserPurchased(userPurchased: UserPurchasedModel): Promise<any> {

    return new Promise<any>(async (resolve) => {

      const object = userPurchased.serialize();

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'business',
        url: 'user-purchaseds',
        subUrl: '/' + object.id.toString(),
        params: {
          type: 'put',
          body: object
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((result) => {

        console.log(result);

        this.notificationService.addToStream({
          title: 'Покупка: ' + userPurchased.id,
          body: 'Успішно записано.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve(result);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Пропозиція',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(0);

      });

    });

  }

  async saveValidity(validity: ValidityOfferModel): Promise<any> {

    return new Promise<any>(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'business',
        url: 'offer-validity',
        subUrl: validity._isNew ? '' : '/' + validity.id.toString(),
        params: {
          type: validity._isNew ? 'post' : 'put',
          body: validity.serialize()
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((result) => {

        console.log(result);

        this.notificationService.addToStream({
          title: 'Термін дії пропозиції: ' + validity.id,
          body: 'Успішно записано.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve(result);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Пропозиція',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(0);

      });

    });

  }

  async saveCode(code: CodeModel): Promise<any> {

    return new Promise<any>(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'business',
        url: 'offer-codes',
        params: {
          type: 'post',
          body: code.serialize()
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((result) => {

        console.log(result);

        this.notificationService.addToStream({
          title: 'Код пропозиції: ' + code.code,
          body: 'Успішно записано.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve(result);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Пропозиція',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(0);

      });

    });

  }

  async saveLink(link: LinkModel): Promise<any> {

    return new Promise<any>(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'business',
        url: 'offer-links',
        params: {
          type: 'post',
          body: link.serialize()
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((result) => {

        console.log(result);

        this.notificationService.addToStream({
          title: 'Код пропозиції: ' + link.link,
          body: 'Успішно записано.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve(result);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Пропозиція',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(0);

      });

    });

  }
}
