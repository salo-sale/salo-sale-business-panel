import {Component, ElementRef, forwardRef, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AdminPanelService} from '../../../../../shared/services/admin-panel.service';
import {CompanyService} from '../../../shared/services/company.service';
import {CompanyModel} from '../../../shared/models/company.model';
import {isNullOrUndefined} from 'util';
import {ImageCroppedEvent} from 'ngx-image-cropper';
import {ConvertTool} from '../../../../../../../shared/tools/convert.tool';
import {environment} from '../../../../../../../../environments/environment';
import {AppService} from '../../../../../../../shared/services/app.service';
import {CompanyStatusEnum} from '../../../../../../../shared/enums/status/company.status.enum';

@Component({
  selector: 'app-view-page',
  templateUrl: './view-page.component.html',
  styleUrls: ['./view-page.component.scss']
})
export class ViewPageComponent implements OnInit, OnDestroy {

  id = 'company-view';
  pageId = 'app-admin-company-view-view';

  loading = true;
  public paramID: string;

  //
  // Start: Image cropper
  //

  imageChangedEvent: any = null;
  croppedImage: any = null;

  @ViewChild('bannerInput')
  bannerInputVar: ElementRef;

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    // console.log(event.file);
    // console.log(event.base64);
    // console.log(ConvertTool.b64toBlob(event.base64));
  }
  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // TODO request for save logo
    // show message
  }

  //
  // Finish: Image cropper
  //

  constructor(
    @Inject(forwardRef(() => AdminPanelService)) adminService,
    private activatedRoute: ActivatedRoute,
    public companyService: CompanyService,
    public appService: AppService
  ) {
    adminService.titlePage = 'Перегляд закладу';

  }

  statusName(status) {

    return CompanyStatusEnum[status];

  }

  get company(): CompanyModel {

    return this.companyService.selectedCompany;

  }

  ngOnDestroy(): void {

    this.companyService.selectedCompany = null;

  }

  ngOnInit() {

    this.activatedRoute.parent.firstChild.params.subscribe(async (params) => {

      this.paramID = params.id;
      this.companyService.getCompany(this.paramID).then(() => {

        this.loader(false);

      });

    });

  }

  clickOnRefreshBtn() {

    if (isNullOrUndefined(this.paramID)) {
      return;
    }

    this.loader(true);
    this.companyService.getCompany(this.paramID).then(() => {

      this.loader(false);

    });

  }

  loader(run: boolean) {
    this.loading = run;
  }

  tagName(id) {

    const tag = this.appService.listsOfTags.find((list) => list.name === environment.defaultDate.objects.company).tags.find((tag) => Number(tag.id.toString()) === Number(id));
    return isNullOrUndefined(tag) || isNullOrUndefined(tag.name) ? 'No data' : tag.name;

  }

  saveUploadImage() {

    // console.log(this.croppedImage);
    // console.log(this.imageChangedEvent);

    this.companyService.saveLogo(this.company, ConvertTool.blobToFile(ConvertTool.b64toBlob(this.croppedImage), 'logo.jpg')).then((result) => {

      if (result) {

        console.log(this.imageChangedEvent);

        this.imageChangedEvent.path[0].value = null;
        this.imageChangedEvent = null;

        const img = document.getElementById('app-admin-panel-company-view-logo');

        img.setAttribute('src', img.getAttribute('src') + '?' + (new Date()).getTime().toString());

      }

    });
    this.croppedImage = null;
  }

  clearNewImg() {

    this.croppedImage = null;
    this.imageChangedEvent = null;
    this.bannerInputVar.nativeElement.value = '';

  }

}
