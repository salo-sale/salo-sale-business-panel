import {ChangeDetectorRef, Component, Input, NgZone, OnInit} from '@angular/core';
import {ContactModel} from '../../../shared/models/contact.model';
import {CompanyService} from '../../../shared/services/company.service';
import {isNullOrUndefined, isNumber} from 'util';
import {environment} from '../../../../../../../../environments/environment';
import {CompanyModel} from '../../../shared/models/company.model';
import {MemberModel} from '../../../shared/models/member.model';
import { MemberStatusEnum } from 'src/app/shared/enums/status/member.status';
import {DeviceDetectorService} from 'ngx-device-detector';
import {MemberInterface} from '../../../shared/models/interfaces/member.interface';
import {QrCodeModel} from '../../../shared/models/qr-code.model';

@Component({
  selector: 'app-admin-panel-company-module-qr-code-component',
  templateUrl: './qr-code.component.html',
  styleUrls: ['./qr-code.component.scss']
})
export class QrCodeComponent implements OnInit {
  id = 'company-qr-code';
  pageId = 'app-admin-company-qr-code-home';

  public initialized = false;
  public initializedForm = false;
  public initHideBody = true;
  public collapseContent = true;
  public loading = true;

  isDesktop = false;
  @Input()
  companyId: number = null;
  selectedItem: QrCodeModel;

  constructor(
    public companyService: CompanyService,
    private deviceService: DeviceDetectorService
  ) {

    this.selectedItem = (new QrCodeModel).initModel();
    this.selectedItem.company_id = this.companyId;
    this.selectedItem.initForm();
    this.initializedForm = true;
    this.isDesktop = deviceService.isDesktop();

  }

  get filterQrCode() {

    return this.companyService.filterQrCode;

  }

  getStatusName(status: number) {
    return MemberStatusEnum[status];
  }

  get qrCodes(): QrCodeModel[] {

    return this.companyService.qrCodes;

  }

  doFiltering(initPageList: boolean = true) {

    this.loader(true);

    this.companyService.getQrCodes(true).then(() => {

      if (initPageList) {

        this.companyService.filterQrCode.initPageList();

      }

      this.loader(false);

    });

  }

  type(type) {

    return isNullOrUndefined(type) ? environment.typeQrCode : environment.typeQrCode[type];

  }

  ngOnInit() {

    this.filterQrCode.companyId = this.companyId;

    if (isNullOrUndefined(this.companyId)) {

      this.initHideBody = false;

    } else {

      this.initHideBody = true;

    }

    this.collapseContent = !this.initHideBody;

    this.init();

    if (this.initHideBody) {

      this.loader(false);

    } else {

      this.doFiltering();

    }

  }


  clickOnRefreshBtn() {

    this.doFiltering();

  }

  clickOnCollapseBtn() {

    this.collapseContent = !this.collapseContent;

    console.log(this.filterQrCode.total);

    if (this.filterQrCode.total === 0 && this.collapseContent) {

      this.doFiltering();

    }

  }

  private init() {

    if (!this.initialized) {

      this.initialized = true;


    }

  }

  loader(run: boolean) {
    this.loading = run;
  }

  initSelectedItem(selectedItem = new QrCodeModel().initModel()) {

    this.initializedForm = false;
    this.selectedItem = selectedItem;
    this.selectedItem.company_id = this.companyId;
    this.selectedItem.initForm();
    this.initializedForm = true;
    console.log(this.selectedItem.formGroup);

  }

  saveMember() {

    if (this.selectedItem.validForm) {

      console.log(this.selectedItem);

      this.companyService.saveQrCode(this.selectedItem).then((success) => {

        if (success) {

          this.doFiltering();

        }

      });
      console.log(this.selectedItem);

    }

  }

  deleteMember() {

    if (confirm('Чи напевно хочеш видалити?') === true) {


      if (!this.selectedItem._isNew) {

        this.companyService.deleteQrCode(this.selectedItem.id).then((result) => {

          this.doFiltering();
          this.initSelectedItem();

        });

      }

    }

  }
}
