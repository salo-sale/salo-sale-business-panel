import { Component, OnInit } from '@angular/core';
import {CompanyModel} from '../../../shared/models/company.model';
import {CompanyService} from '../../../shared/services/company.service';
import {environment} from '../../../../../../../../environments/environment';
import {ActivatedRoute, Route, Router} from '@angular/router';
import {isBoolean, isNullOrUndefined, isNumber} from 'util';
import {PointOfSaleModel} from '../../../../admin-panel-point-of-sale/shared/models/point-of-sale.model';
import {PointOfSaleService} from '../../../../admin-panel-point-of-sale/shared/services/point-of-sale.service';
import {AppService} from '../../../../../../../shared/services/app.service';

@Component({
  selector: 'app-admin-panel-company-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  model: CompanyModel;
  submitted = false;
  pointsOfSale: PointOfSaleModel[];

  public tags: {id: number, name: string}[];

  constructor(
    public activatedRoute: ActivatedRoute,
    public pointOfSaleService: PointOfSaleService,
    public companyService: CompanyService,
    public appService: AppService,
    public router: Router
  ) {

    this.model = null;
    this.pointsOfSale = [];

  }


  ngOnInit() {

    this.activatedRoute.parent.firstChild.params.subscribe(async (params) => {

      if (!isNullOrUndefined(Number(params.id)) && !isNaN(Number(params.id))) {

        if (isNullOrUndefined(this.companyService.selectedCompany)) {

          await this.companyService.getCompany(params.id);

        }

        this.model = this.companyService.selectedCompany;

      } else {

        this.model = new CompanyModel();

      }

      this.model.initForm();

      this.tags = this.appService.listsOfTags.find((list) => list.name === environment.defaultDate.objects.company).tags.map((tag) => {

        return {
          id: Number(tag.id),
          name: tag.name
        };

      });


    });

  }

  changed(data) {

    console.log(data);

    // this.model.tags = data.value.map(value => parseInt(value));

  }

  async saveDocument(afterSaveGoToHome = false, afterSaveGoToNextNewDocument = false) {

    this.submitted = true;

    if (this.model.validForm) {

      this.submitted = false;

      let canSave: boolean | number = true;

      if (this.model._isNew && this.pointsOfSale.length > 0) {

        await this.pointsOfSale.forEach((pos) => {

          if (pos.validForm) {

            console.log('ok');

          } else {

            console.error(pos);
            canSave = false;

          }

        });

      }

      if (canSave) {

        console.log(this.model.serialize());
        canSave = await this.companyService.saveCompany(this.model);

        if (canSave > 0) {

          if (this.pointsOfSale.length > 0) {

            await this.pointsOfSale.forEach(async (pos) => {

              pos.companyId = canSave as number;

              await this.pointOfSaleService.savePointOfSale(pos);

            });

          }


          if (afterSaveGoToHome) {

            this.router.navigate(['business', 'company']);

          } else if (afterSaveGoToNextNewDocument) {

            this.model = new CompanyModel();
            this.model.initForm();
            this.router.navigate(['business', 'company', 'create']);

          } else {


            this.router.navigate(['business', 'company', 'view', canSave]);

          }

        }

      }

    } else {

      console.warn(this.model.formGroup);
      console.warn(this.model);

    }

  }

  addNewPointOfSaleToList() {

    const model = new PointOfSaleModel();
    model.initForm();
    this.pointsOfSale.push(model);

  }

  removeItem(index: number) {

    console.log(index);
    this.pointsOfSale.splice(index, 1);

  }

}
