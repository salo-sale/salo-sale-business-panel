import {CompanyInterface} from './interfaces/company.interface';
import {DeserializableInterface} from '../../../../../../shared/models/interfaces/deserializable.interface';
import {SerializableInterface} from '../../../../../../shared/models/interfaces/serializable.interface';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {isNullOrUndefined} from 'util';
import {ImageInterface} from '../../../../shared/models/interfaces/image.interface';

export class CompanyModel implements CompanyInterface, DeserializableInterface<CompanyInterface>, SerializableInterface<CompanyInterface> {

  /**
   *
   * Declaration variables
   *
   */

  private formBuilder: FormGroup;

  id: number;
  ikoint: number;
  latinName: string;
  motto: string;
  name: string;
  primaryColor: string;
  shortName: string;
  status: number;
  updatedAt: string;
  tags: number[];
  limitCreateQrCode: number;
  createdQuantityQrCode: number;

  _isNew: boolean;
  logoUrl: ImageInterface;

  constructor() {

    this._isNew = true;
    this.status = 1;
    this.primaryColor = '#ff9700';
    this.initForm();

  }

  deserialize(input: CompanyInterface): this {

    this._isNew = false;

    return Object.assign(this, input);

  }

  serialize(): CompanyInterface {

    const object = Object.assign({} as CompanyInterface, this);
    delete object._isNew;
    delete object.ikoint;
    delete object.updatedAt;
    delete object.logoUrl;
    delete object.formBuilder;
    if (object.primaryColor.indexOf('#') > -1) {
      object.primaryColor = object.primaryColor.substr(1);
    }

    return object;

  }

  get formGroup(): FormGroup {

    return this.formBuilder;

  }

  get validForm(): boolean {

    if (this.formGroup.valid) {

      this.name = this.formGroup.get('name').value;
      this.tags = this.formGroup.get('tags').value;
      this.status = this.formGroup.get('status').value;
      this.shortName = this.formGroup.get('shortName').value;
      this.latinName = this.formGroup.get('latinName').value;
      this.motto = this.formGroup.get('motto').value;
      // this.primaryColor = this.formGroup.get('primaryColor').value.substring(1);

      return true;

    } else {

      return false;

    }

  }

  public initForm(): void {

    if (!isNullOrUndefined(this.tags)) {

      this.tags = this.tags.map(tag => Number(tag));

    }

    if (!this.primaryColor.includes('#')) {

      this.primaryColor = '#' + this.primaryColor;

    }

    this.formBuilder = new FormBuilder().group({
      name: [
        this.name,
        [
          Validators.required,
        ]
      ],
      shortName: [
        this.shortName,
      ],
      tags: [
        isNullOrUndefined(this.tags) ? null : this.tags.map(tag => Number(tag)),
      ],
      latinName: [
        this.latinName,
      ],
      motto: [
        this.motto,
      ],
      status: [
        this.status,
      ],
      primaryColor: [
        this.primaryColor,
        [
          Validators.required,
        ]
      ],
    });

  }

}
