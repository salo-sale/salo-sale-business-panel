export interface ContactInterface {

  id: number;
  hideValue: number;
  companyId: number;
  name: string;
  value: string;
  type: number;
  status: number;

}
