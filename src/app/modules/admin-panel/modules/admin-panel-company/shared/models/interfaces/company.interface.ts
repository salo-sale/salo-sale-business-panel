import {ImageInterface} from '../../../../../shared/models/interfaces/image.interface';

export interface CompanyInterface {

  id: number;
  name: string;
  shortName: string;
  motto: string;
  latinName: string;
  primaryColor: string;
  status: number;
  ikoint: number;
  logoUrl: ImageInterface;
  updatedAt: string;
  tags: number[];
  limitCreateQrCode: number;
  createdQuantityQrCode: number;

}
