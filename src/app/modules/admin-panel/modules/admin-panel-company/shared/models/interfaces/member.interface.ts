export interface MemberInterface {

  id: number;
  companyId: number;
  userId: number;
  role: number;
  status: number;
  createdAt: string;
  updatedAt: string;
  userEmail: string;


}
