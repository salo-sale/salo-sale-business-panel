import {ContactInterface} from './interfaces/contact.interface';
import {DeserializableInterface} from '../../../../../../shared/models/interfaces/deserializable.interface';
import {SerializableInterface} from '../../../../../../shared/models/interfaces/serializable.interface';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {isNullOrUndefined} from "util";

export class ContactModel implements ContactInterface, DeserializableInterface<ContactInterface>, SerializableInterface<ContactInterface> {

  /**
   *
   * Declaration variables
   *
   */

  private formBuilder: FormGroup;

  companyId: number;
  id: number;
  hideValue: number;
  name: string;
  status: number;
  type: number;
  value: string;

  _isNew: boolean;

  constructor() {

    this.type = 0;
    this.status = 1;
    this.hideValue = 0;
    this._isNew = true;

  }

  deserialize(input: ContactInterface): this {

    this._isNew = false;

    const obj1 = Object.keys(input);

    for (const keyObj1 of obj1) {

      if (input[keyObj1] !== undefined && input[keyObj1] !== null) {

        const type = this[keyObj1];
        switch (typeof type) {
          case 'undefined':
            break;
          case 'object':
            break;
          case 'boolean':
            input[keyObj1] = Boolean(Number(input[keyObj1].toString()));
            break;
          case 'number':
            input[keyObj1] = Number(input[keyObj1].toString());
            break;
          case 'string':
            break;
          case 'function':
            break;
          case 'symbol':
            break;
          case 'bigint':
            // @ts-ignore
            input[keyObj1] = BigInt(input[keyObj1].toString());
            break;

        }

      }

    }

    return Object.assign(this, input);

  }

  serialize(): ContactInterface {

    const object = Object.assign({} as ContactInterface, this);
    delete object._isNew;
    delete object.formBuilder;

    return object;

  }

  get formGroup(): FormGroup {

    return this.formBuilder;

  }

  get validForm(): boolean {

    if (this.formGroup.valid) {

      Object.keys(this.formGroup.value).forEach(key => {

        if (!isNullOrUndefined(this.formGroup.value[key])) {

          this[key] = this.formGroup.value[key];

        }

      });

      this.hideValue = this.hideValue ? 1 : 0;

      return true;

    } else {

      return false;

    }

  }

  public initForm(): void {

    console.log(this.status);

    this.formBuilder = new FormBuilder().group({
      name: [
        this.name
      ],
      hideValue: [
        this.hideValue
      ],
      type: [
        this.type,
        [
          Validators.required,
        ]
      ],
      value: [
        this.value,
        [
          Validators.required,
        ]
      ],
      status: [
        this._isNew ? 1 : Number(this.status.toString())
      ],
    });

  }

}
