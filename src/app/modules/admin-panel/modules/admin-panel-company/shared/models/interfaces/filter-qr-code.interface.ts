export interface FilterQrCodeInterface {

  page: number;
  count: number;
  orderBy: string;
  sort: string;
  expand: string;
  status: number;
  companyId: number;

}
