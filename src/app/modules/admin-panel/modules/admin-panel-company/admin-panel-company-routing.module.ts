import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AdminPanelCompanyComponent} from './admin-panel-company.component';
import {HomePageComponent} from './ui/pages/home-page/home-page.component';
import {ViewPageComponent} from './ui/pages/view-page/view-page.component';
import {CreatePageComponent} from './ui/pages/create-page/create-page.component';
import {UpdatePageComponent} from './ui/pages/update-page/update-page.component';

export const routes: Routes = [{
  path: '',
  component: AdminPanelCompanyComponent,
  children: [{
    path: '',
    component: HomePageComponent
  }, {
    path: 'view/:id',
    component: ViewPageComponent
  }, {
    path: 'create',
    component: CreatePageComponent
  }, {
    path: 'update/:id',
    component: UpdatePageComponent
  }]
}];


@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class AdminPanelCompanyRoutingModule { }
