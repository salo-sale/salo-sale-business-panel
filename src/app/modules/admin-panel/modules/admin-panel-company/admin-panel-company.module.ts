import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminPanelCompanyComponent } from './admin-panel-company.component';
import { HomePageComponent } from './ui/pages/home-page/home-page.component';
import {AdminPanelCompanyRoutingModule} from './admin-panel-company-routing.module';
import { FormComponent } from './ui/components/form/form.component';
import { CreatePageComponent } from './ui/pages/create-page/create-page.component';
import { UpdatePageComponent } from './ui/pages/update-page/update-page.component';
import { ViewPageComponent } from './ui/pages/view-page/view-page.component';
import {CompanyService} from './shared/services/company.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AdminPanelOfferModule} from '../admin-panel-offer/admin-panel-offer.module';
import {AdminPanelPointOfSaleModule} from '../admin-panel-point-of-sale/admin-panel-point-of-sale.module';
import {ImageCropperModule} from 'ngx-image-cropper';
import {AdminPanelContractModule} from '../admin-panel-contract/admin-panel-contract.module';
import {ColorPickerModule} from 'ngx-color-picker';
import { ContactsComponent } from './ui/components/contacts/contacts.component';
import {NgSelectModule} from '@ng-select/ng-select';
import {AdminPanelAdvertisingBannerModule} from '../admin-panel-advertising-banner/admin-panel-advertising-banner.module';
import { MembersComponent } from './ui/components/members/members.component';
import {KarbaCoreModule} from '../../shared/modules/karba-core/karba-core.module';
import {QrCodeComponent} from './ui/components/qr-code/qr-code.component';
import {AdminPanelUserModule} from '../admin-panel-user/admin-panel-user.module';

@NgModule({
  declarations: [
    AdminPanelCompanyComponent,
    HomePageComponent,
    FormComponent,
    CreatePageComponent,
    UpdatePageComponent,
    ViewPageComponent,
    ContactsComponent,
    QrCodeComponent,
    MembersComponent
  ],
  providers: [
    CompanyService,
  ],
    imports: [
        CommonModule,
        AdminPanelCompanyRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        AdminPanelOfferModule,
        AdminPanelPointOfSaleModule,
        ImageCropperModule,
        AdminPanelContractModule,
        ColorPickerModule,
        NgSelectModule,
        AdminPanelAdvertisingBannerModule,
        KarbaCoreModule,
        AdminPanelUserModule
    ]
})
export class AdminPanelCompanyModule {

  constructor(
    private companyService: CompanyService
  ) {

    companyService.init();

  }


}
