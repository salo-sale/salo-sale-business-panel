import {WorkHoursInterface} from './work-hours.interface';
import {LanguageInterface} from './languageInterface';

export interface PointOfSaleInterface {

  id: number;
  companyId: number;
  companyName: string;
  localityId: number;
  comment: string;
  ikoint: number;
  status: number;
  longitude: number;
  latitude: number;
  languages: LanguageInterface[];
  workHours: WorkHoursInterface[];

}
