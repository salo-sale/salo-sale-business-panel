import {DeserializableInterface} from '../../../../../../shared/models/interfaces/deserializable.interface';
import {SerializableInterface} from '../../../../../../shared/models/interfaces/serializable.interface';
import {MemberInterface} from './interfaces/member.interface';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {isNullOrUndefined} from 'util';

export class MemberModel implements MemberInterface, DeserializableInterface<MemberInterface>, SerializableInterface<MemberInterface> {

  private formBuilder: FormGroup;

  pointOfSaleId: number;
  createdAt: string;
  id: number;
  role: number;
  status: number;
  userEmail: string;
  updatedAt: string;
  userId: number;

  _isNew: boolean;

  constructor() {
  }

  public initModel() {

    this.role = 3;
    this.status = 1;
    this.pointOfSaleId = 0;
    this.createdAt = '';
    this.id = 0;
    this.userEmail = '';
    this.updatedAt = '';
    this.userId = 0;

    this._isNew = true;

    return this;

  }

  deserialize(input: MemberInterface): this {

    this._isNew = false;

    const obj1 = Object.keys(input);

    for (const keyObj1 of obj1) {

      if (input[keyObj1] !== undefined && input[keyObj1] !== null) {

        const type = this[keyObj1];
        switch (typeof type) {
          case 'undefined':
            break;
          case 'object':
            break;
          case 'boolean':
            input[keyObj1] = Boolean(Number(input[keyObj1].toString()));
            break;
          case 'number':
            input[keyObj1] = Number(input[keyObj1].toString());
            break;
          case 'string':
            break;
          case 'function':
            break;
          case 'symbol':
            break;
          case 'bigint':
            // @ts-ignore
            input[keyObj1] = BigInt(input[keyObj1].toString());
            break;

        }

      }

    }

    return Object.assign(this, input);

  }

  serialize(): MemberInterface {

    const object = Object.assign({} as MemberInterface, this);
    delete object._isNew;
    delete object.formBuilder;

    return object;

  }

  get formGroup(): FormGroup {

    return this.formBuilder;

  }

  get validForm(): boolean {

    if (this.formGroup.valid) {

      Object.keys(this.formGroup.value).forEach(key => {

        if (!isNullOrUndefined(this.formGroup.value[key])) {

          this[key] = this.formGroup.value[key];

        }

      });

      return true;

    } else {

      return false;

    }

  }

  initForm(): boolean {

    console.log(this);

    this.formBuilder = new FormBuilder().group({
      status: [
        this.status
      ],
      role: [
        this.role
      ],
      userEmail: [
        this.userEmail,
        [
          Validators.required,
        ]
      ],
    });

    return true;

  }

}
