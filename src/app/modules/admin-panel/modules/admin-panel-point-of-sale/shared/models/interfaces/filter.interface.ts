export interface FilterInterface {

  companyId: number;
  offerId: number;
  page: number;
  count: number;
  orderBy: string;
  sort: string;
  status: number;
  search: string;

}
