export interface WorkHoursInterface {

  id: number;
  pointOfSaleId: number;
  openTime: number;
  closeTime: number;
  weekday: number;
  canUse: number;
  isRange: number;
  status: number;
  isFrom: number;
  workDate: {year: number, month: number, day: number} & Date | Date;

}
