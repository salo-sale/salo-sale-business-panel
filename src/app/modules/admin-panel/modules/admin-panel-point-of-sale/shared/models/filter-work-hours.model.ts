
import {DeserializableInterface} from '../../../../../../shared/models/interfaces/deserializable.interface';
import {SerializableInterface} from '../../../../../../shared/models/interfaces/serializable.interface';
import {environment} from '../../../../../../../environments/environment';
import {FilterWorkHoursInterface} from "./interfaces/filter-work-hours.interface";
import {ConvertTool} from '../../../../../../shared/tools/convert.tool';

export class FilterWorkHoursModel implements FilterWorkHoursInterface, DeserializableInterface<FilterWorkHoursInterface>, SerializableInterface<FilterWorkHoursInterface> {

  count: number;
  orderBy: string;
  page: number;
  sort: string;

  status: number;
  accessForUser: number;
  accessForUse: number;
  type: number;
  companyId: number;
  limit: number;
  search: string;

  total: number;
  pointOfSaleId: number;
  pageList: number[];

  constructor() {

    this.initModel();

  }

  /**
   *
   * @param input
   */
  deserialize(input: FilterWorkHoursInterface): this {

    return Object.assign(this, input);

  }

  /**
   *
   */
  serialize(): FilterWorkHoursInterface {

    return ConvertTool.clearObject<FilterWorkHoursInterface>(Object.assign({} as FilterWorkHoursInterface, this));

  }

  /**
   * Function init local global variables with start data from environment
   */
  initModel() {

    this.count = environment.filter.count;
    this.page = environment.filter.page;
    this.sort = environment.filter.sort;
    this.search = '';
    this.orderBy = 'id';
    this.status = null;

    this.total = 0;
    this.pageList = [];

  }

  /**
   *
   */
  get actualPage(): number {

    return this.page;

  }

  /**
   *
   */
  get lastPage(): number {

    return Math.ceil((this.total / this.count));

  }

  /**
   *
   */
  get actualPageIsLastPage(): boolean {

    return this.page === this.lastPage;

  }

  /**
   *
   */
  get actualPageIsFirstPage(): boolean {

    return this.page === 1;

  }


  /**
   *
   */
  initPageList() {

    this.pageList = [];

    console.log(this.total);

    if (this.total > 0) {

      console.log(this.pageList);

      let localCurrentIndexPage = this.page;
      let localMaxPage = this.actualPage + 2;

      if (localCurrentIndexPage > 2) {

        localCurrentIndexPage -= 2;

      } else if (localCurrentIndexPage > 1) {

        localCurrentIndexPage -= 1;

      }

      if (this.actualPage === this.lastPage) {

        localMaxPage = this.actualPage;

      } else if (this.lastPage - this.actualPage === 1) {

        localMaxPage = this.actualPage + 1;

      }

      for (let i = localCurrentIndexPage; i <= localMaxPage; i++) {

        this.pageList.push(i);

      }

    }

  }


  /**
   *
   */
  toggleOrderDir() {

    this.sort = this.sort === 'DESC' ? 'ASC' : 'DESC';
    this.initPageList();

  }

  /**
   *
   * @param column
   * @param toggleOrderDir
   */
  changeOrderBy(column: string, toggleOrderDir: boolean = true) {

    if (toggleOrderDir) {

      if (this.orderBy === column) {

        this.toggleOrderDir();

      }

    }

    this.orderBy = column;
    this.initPageList();

  }


  changePage(page: number) {

    this.page = page;
    this.initPageList();

  }


  goToFirstPage() {

    this.changePage(1);

  }

  goToLastPage() {

    this.changePage(this.lastPage);

  }

  resetPage() {

    this.page = environment.filter.page;

  }

}
