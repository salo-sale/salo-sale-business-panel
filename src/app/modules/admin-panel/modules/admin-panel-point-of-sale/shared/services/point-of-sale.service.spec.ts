import { TestBed } from '@angular/core/testing';

import { PointOfSaleService } from './point-of-sale.service';

describe('PointOfSaleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PointOfSaleService = TestBed.get(PointOfSaleService);
    expect(service).toBeTruthy();
  });
});
