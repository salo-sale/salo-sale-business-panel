import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageComponent } from './ui/pages/home-page/home-page.component';
import {AdminPanelPointOfSaleRoutingModule} from './admin-panel-point-of-sale-routing.module';
import {AdminPanelPointOfSaleComponent} from './admin-panel-point-of-sale.component';
import { FormComponent } from './ui/components/form/form.component';
import { CreatePageComponent } from './ui/pages/create-page/create-page.component';
import { UpdatePageComponent } from './ui/pages/update-page/update-page.component';
import { ViewPageComponent } from './ui/pages/view-page/view-page.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgSelectModule} from '@ng-select/ng-select';
import {ContactsComponent} from './ui/components/contacts/contacts.component';
import {MembersComponent} from './ui/components/members/members.component';
import {WorkHoursComponent} from "./ui/components/work-hours/work-hours.component";
import {NgbDatepickerModule} from "@ng-bootstrap/ng-bootstrap";
import {PipeDayNameModule} from '../../shared/modules/pipe-day-name/pipe-day-name.module';
import {KarbaCoreModule} from '../../shared/modules/karba-core/karba-core.module';

@NgModule({
  declarations: [
    AdminPanelPointOfSaleComponent,
    HomePageComponent,
    FormComponent,
    CreatePageComponent,
    UpdatePageComponent,
    ViewPageComponent,
    ContactsComponent,
    MembersComponent,
    WorkHoursComponent
  ],
  exports: [
    HomePageComponent,
    // GoogleMapsComponent,
    FormComponent
  ],
    imports: [
        CommonModule,
        AdminPanelPointOfSaleRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NgSelectModule,
        NgbDatepickerModule,
        PipeDayNameModule,
        KarbaCoreModule,
    ]
})
export class AdminPanelPointOfSaleModule { }
