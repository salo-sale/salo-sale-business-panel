import {Component, forwardRef, Inject, OnDestroy, OnInit} from '@angular/core';
import {AdminPanelService} from '../../../../../shared/services/admin-panel.service';
import {PointOfSaleService} from '../../../shared/services/point-of-sale.service';
import {PointOfSaleModel} from '../../../shared/models/point-of-sale.model';
import {isNullOrUndefined} from "util";
import {ActivatedRoute} from '@angular/router';
import {AppService} from '../../../../../../../shared/services/app.service';
import {environment} from '../../../../../../../../environments/environment';
import {CompanyStatusEnum} from '../../../../../../../shared/enums/status/company.status.enum';
import {PointOfSaleStatusEnum} from '../../../../../../../shared/enums/status/point-of-sale.status.enum';

@Component({
  selector: 'app-view-page',
  templateUrl: './view-page.component.html',
  styleUrls: ['./view-page.component.scss']
})
export class ViewPageComponent implements OnInit, OnDestroy {

  id = 'point-of-sale-view';
  pageId = 'app-admin-point-of-sale-view-view';

  loading = true;
  public paramID: string;

  constructor(
    @Inject(forwardRef(() => AdminPanelService)) adminService,
    public pointOfSaleService: PointOfSaleService,
    public activatedRoute: ActivatedRoute,
    public appService: AppService,
  ) {
    adminService.titlePage = 'Перегляд торгової точки';
  }

  get pos(): PointOfSaleModel {

    return this.pointOfSaleService.selectedPointOfSale;

  }
  get logo(): string {

    return environment.host + 'uploads/companies/' + this.pos.companyId + '/logo.jpg';

  }

  getStatusName(status) {

    return PointOfSaleStatusEnum[status];

  }

  ngOnInit() {

    this.activatedRoute.parent.firstChild.params.subscribe(async (params) => {

      this.paramID = params.id;
      this.pointOfSaleService.getPointOfSale(this.paramID).then(() => {

        this.loader(false);

      });

    });

  }

  clickOnRefreshBtn() {

    if (isNullOrUndefined(this.paramID)) {
      return;
    }

    this.loader(true);
    this.pointOfSaleService.getPointOfSale(this.paramID).then(() => {

      this.loader(false);

    });

  }

  getCityName(localityId: number) {
    if (isNullOrUndefined(this.appService.cities)) {

      return 'Not found';

    } else {

      const object = this.appService.cities.find((city) => city.id === localityId);
      return  isNullOrUndefined(object) ? 'Not found' : object.name;

    }

  }

  loader(run: boolean) {
    this.loading = run;
  }

  languageName(code) {

    return this.appService.languages.find((language) => language.code === code).name;

  }

  ngOnDestroy(): void {

    this.pointOfSaleService.selectedPointOfSale = null;

  }

}
