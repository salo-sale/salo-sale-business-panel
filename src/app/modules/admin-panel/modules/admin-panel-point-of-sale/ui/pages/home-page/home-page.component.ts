import {Component, forwardRef, Inject, Input, OnDestroy, OnInit} from '@angular/core';
import {PointOfSaleService} from '../../../shared/services/point-of-sale.service';
import {AdminPanelService} from '../../../../../shared/services/admin-panel.service';
import {PointOfSaleModel} from '../../../shared/models/point-of-sale.model';
import {AppService} from '../../../../../../../shared/services/app.service';
import {OfferService} from '../../../../admin-panel-offer/shared/services/offer.service';
import {CityStatusEnum} from '../../../../../../../shared/enums/status/city.status.enum';
import {PointOfSaleStatusEnum} from '../../../../../../../shared/enums/status/point-of-sale.status.enum';
import {isNullOrUndefined} from 'util';
import {DeviceDetectorService} from 'ngx-device-detector';

@Component({
  selector: 'app-admin-panel-point-of-sale-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit, OnDestroy {
  id = 'points-of-sale';
  pageId = 'app-admin-points-of-sale-home';

  public initialized = false;
  public initHideBody = true;
  public collapseContent = true;
  public loading = true;

  isDesktop = false;

  public adminService: AdminPanelService;
  public openLoaderForIndex: number;

  @Input()
  companyId: number = null;

  @Input()
  offerId: number = null;

  @Input()
  pointsOfSaleForOffer: string[] = [];

  constructor(
    @Inject(forwardRef(() => AdminPanelService)) adminService,
    public pointOfSaleService: PointOfSaleService,
    public offerService: OfferService,
    public appService: AppService,
    private deviceService: DeviceDetectorService
  ) {

    this.isDesktop = deviceService.isDesktop();
    this.openLoaderForIndex = null;
    this.adminService = adminService;

  }


  clickOnRefreshBtn() {

    this.doFiltering();

  }

  clickOnCollapseBtn() {

    this.collapseContent = !this.collapseContent;

    if (this.filter.total === 0 && this.collapseContent) {

      this.doFiltering();

    }

  }

  private init() {

    if (!this.initialized) {

      this.initialized = true;


    }

  }

  loader(run: boolean) {
    this.loading = run;
  }


  get filter () {

    return this.pointOfSaleService.filter;

  }

  clearSearch() {

    this.pointOfSaleService.filter.search = '';
    this.doFiltering();

  }

  get pointsOfSale(): PointOfSaleModel[] {

    return this.pointOfSaleService.pointsOfSale;

  }

  doFiltering(initPageList: boolean = true) {

    this.loader(true);


    if (initPageList) {

      this.filter.resetPage();

    }

    // , this.offerId
    this.pointOfSaleService.getPointsOfSale(true, this.companyId).then(() => {

      if (initPageList) {

        this.filter.initPageList();

      }

      this.loader(false);

    });

  }

  ngOnInit() {

    if (isNullOrUndefined(this.companyId) && isNullOrUndefined(this.offerId)) {
      this.adminService.titlePage = 'Торгові точки';
      this.initHideBody = false;
    } else {
      this.initHideBody = true;
    }

    this.collapseContent = !this.initHideBody;

    this.init();

    if (this.initHideBody) {

      this.loader(false);

    } else {

      this.doFiltering();

    }

  }

  selectPOS(pos: PointOfSaleModel) {

    this.pointOfSaleService.selectedPointOfSale = pos;

  }

  getCityName(localityId: number) {
    if (isNullOrUndefined(this.appService.cities)) {

      return 'Not found';

    } else {

      const object = this.appService.cities.find((city) => city.id === localityId);
      return  isNullOrUndefined(object) ? 'Not found' : object.name;

    }

  }

  checkPos(id: string) {

    return this.pointsOfSaleForOffer.findIndex((p) => p === id) > -1;

  }

  getStatusName(status) {

    return PointOfSaleStatusEnum[status];

  }

  togglePosToOffer(pos: PointOfSaleModel, remove = false, index) {

    this.openLoaderForIndex = index;

    this.pointOfSaleService.togglePosToOffer(this.offerId, pos, remove).then(result => {

      console.log(result);
      if (result) {

        this.offerService.getOffer(this.offerId.toString()).then(() => {

          this.openLoaderForIndex = null;

        });

      }

    });
  }

  ngOnDestroy(): void {

    this.pointOfSaleService.init();


  }

}
