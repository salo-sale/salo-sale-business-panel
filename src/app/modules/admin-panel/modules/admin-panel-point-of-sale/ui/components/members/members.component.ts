import {Component, Input, OnInit} from '@angular/core';
import {isNullOrUndefined, isNumber} from 'util';
import {environment} from '../../../../../../../../environments/environment';
import {PointOfSaleService} from '../../../shared/services/point-of-sale.service';
import {MemberModel} from '../../../shared/models/member.model';
import {PointOfSaleModel} from '../../../shared/models/point-of-sale.model';
import { MemberStatusEnum } from 'src/app/shared/enums/status/member.status';
import {DeviceDetectorService} from 'ngx-device-detector';

@Component({
  selector: 'app-admin-panel-point-of-sale-module-members-component',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss']
})
export class MembersComponent implements OnInit {
  id = 'point-of-sale-members';
  pageId = 'app-admin-point-of-sale-members-home';

  public initialized = false;
  public initializedForm = false;
  public initHideBody = true;
  public collapseContent = true;
  public loading = true;

  isDesktop = false;
  @Input()
  pointOfSaleId: number = null;
  selectedItem: MemberModel;

  constructor(
    public pointOfSaleService: PointOfSaleService,
    private deviceService: DeviceDetectorService
  ) {

    this.isDesktop = deviceService.isDesktop();
    this.selectedItem = new MemberModel().initModel();
    this.selectedItem.pointOfSaleId = this.pointOfSaleId;
    this.initializedForm = true;
    this.selectedItem.initForm();

  }


  clickOnRefreshBtn() {

    this.doFiltering();

  }

  clickOnCollapseBtn() {

    this.collapseContent = !this.collapseContent;

    if (this.filterMember.total === 0 && this.collapseContent) {

      this.doFiltering();

    }

  }

  private init() {

    if (!this.initialized) {

      this.initialized = true;


    }

  }

  loader(run: boolean) {
    this.loading = run;
  }

  get filterMember() {

    return this.pointOfSaleService.filterMember;

  }

  get members(): MemberModel[] {

    return this.pointOfSaleService.members;

  }

  getStatusName(status: number) {
    return MemberStatusEnum[status];
  }

  doFiltering(initPageList: boolean = true) {

    this.loader(true);

    if (initPageList) {

      this.pointOfSaleService.filterMember.resetPage();

    }

    this.pointOfSaleService.getMembers(true, this.pointOfSaleId).then(() => {

      if (initPageList) {

        this.pointOfSaleService.filterContact.initPageList();

      }

      this.loader(false);

    });

  }

  typeMember(role: number) {

    return isNullOrUndefined(role) ? environment.rolesPointOfSale : environment.rolesPointOfSale[role];

  }

  ngOnInit() {


    this.initHideBody = true;
    this.collapseContent = !this.initHideBody;

    this.init();

    if (this.initHideBody) {

      this.loader(false);

    } else {

      this.doFiltering();

    }

  }

  initSelectedItem(selectedItem = new MemberModel().initModel()) {

    this.initializedForm = false;
    this.selectedItem = selectedItem;
    this.selectedItem.initForm();
    this.initializedForm = true;
    console.log(this.selectedItem.formGroup);

  }

  saveMember() {

    if (this.selectedItem.validForm) {

      this.selectedItem.pointOfSaleId = this.pointOfSaleId;
      this.pointOfSaleService.saveMember(this.selectedItem).then((success) => {

        if (success > 0) {

          this.doFiltering();

        }

      });
      console.log(this.selectedItem);

    }

  }

  deleteMember() {
    if (confirm('Чи напевно хочеш видалити?') === true) {


      if (!this.selectedItem._isNew) {

        this.pointOfSaleService.deleteMember(this.selectedItem.id).then((result) => {

          this.doFiltering();
          this.initSelectedItem();

        });

      }

    }

  }

}
