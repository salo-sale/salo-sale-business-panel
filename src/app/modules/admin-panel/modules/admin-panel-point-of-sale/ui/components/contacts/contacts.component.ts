import {Component, forwardRef, Inject, Input, OnInit} from '@angular/core';
import {isNullOrUndefined, isNumber} from 'util';
import {environment} from '../../../../../../../../environments/environment';
import {PointOfSaleService} from '../../../shared/services/point-of-sale.service';
import {ContactModel} from '../../../../admin-panel-company/shared/models/contact.model';
import {PointOfSaleModel} from '../../../shared/models/point-of-sale.model';
import { ContactStatusEnum } from 'src/app/shared/enums/status/contact.status';
import {DeviceDetectorService} from 'ngx-device-detector';

@Component({
  selector: 'app-admin-panel-point-of-sale-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit {
  id = 'point-of-sale-contacts';
  pageId = 'app-admin-point-of-sale-contacts-home';

  public initialized = false;
  public initHideBody = true;
  public collapseContent = true;
  public loading = true;

  isDesktop = false;

  @Input()
  pointOfSaleId: number = null;
  selectedItem: ContactModel;

  constructor(
    public pointOfSaleService: PointOfSaleService,
    private deviceService: DeviceDetectorService
  ) {

    this.isDesktop = deviceService.isDesktop();
    this.selectedItem = new ContactModel();
    this.selectedItem.initForm();

  }

  get filterContact() {

    return this.pointOfSaleService.filterContact;

  }

  getStatusName(status: number) {
    return ContactStatusEnum[status];
  }

  get contacts(): ContactModel[] {

    return this.pointOfSaleService.contacts;

  }

  doFiltering(initPageList: boolean = true) {

    this.loader(true);

    if (initPageList) {

      this.pointOfSaleService.filterContact.resetPage();

    }

    this.pointOfSaleService.getContacts(true, this.pointOfSaleId).then(() => {

      if (initPageList) {

        this.pointOfSaleService.filterContact.initPageList();

      }

      this.loader(false);

    });

  }

  typeContact(type) {

    return isNullOrUndefined(type) ? environment.typeContact : environment.typeContact[Number(type)];

  }


  clickOnRefreshBtn() {

    this.doFiltering();

  }

  clickOnCollapseBtn() {

    this.collapseContent = !this.collapseContent;

    if (this.filterContact.total === 0 && this.collapseContent) {

      this.doFiltering();

    }

  }

  private init() {

    if (!this.initialized) {

      this.initialized = true;


    }

  }

  loader(run: boolean) {
    this.loading = run;
  }

  ngOnInit() {

    this.initHideBody = true;
    this.collapseContent = !this.initHideBody;

    this.init();

    if (this.initHideBody) {

      this.loader(false);

    } else {

      this.doFiltering();

    }

  }

  selectCompany(company: PointOfSaleModel) {

    this.pointOfSaleService.selectedPointOfSale = company;

  }

  initSelectedItem(selectedItem = new ContactModel()) {

    this.selectedItem = selectedItem;
    this.selectedItem.initForm();
    console.log(this.selectedItem.formGroup);

  }

  saveContact() {

    if (this.selectedItem.validForm) {

      console.log(this.selectedItem);

      this.selectedItem.companyId = this.pointOfSaleId;
      this.pointOfSaleService.saveContact(this.selectedItem).then((result) => {

        if (isNumber(result)) {

          this.doFiltering();

        }

      });
      console.log(this.selectedItem);

    }

  }

  deleteContact() {

    if (!this.selectedItem._isNew) {

      this.pointOfSaleService.deleteContact(this.selectedItem.id).then((result) => {

        this.doFiltering();
        this.initSelectedItem();

      });

    }

  }

}
