import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomePageComponent} from './ui/pages/home-page/home-page.component';
import {AdminPanelPointOfSaleComponent} from './admin-panel-point-of-sale.component';
import {CreatePageComponent} from './ui/pages/create-page/create-page.component';
import {UpdatePageComponent} from './ui/pages/update-page/update-page.component';
import {ViewPageComponent} from './ui/pages/view-page/view-page.component';

export const routes: Routes = [{
  path: '',
  component: AdminPanelPointOfSaleComponent,
  children: [{
    path: '',
    component: HomePageComponent
  }, {
    path: 'create/:companyId',
    component: CreatePageComponent
  }, {
    path: 'create',
    component: CreatePageComponent
  }, {
    path: 'update/:id',
    component: UpdatePageComponent
  }, {
    path: 'update/:id/:companyId',
    component: UpdatePageComponent
  }, {
    path: 'view/:id',
    component: ViewPageComponent
  }]
}];


@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class AdminPanelPointOfSaleRoutingModule { }
