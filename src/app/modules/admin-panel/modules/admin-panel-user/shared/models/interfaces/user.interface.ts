export interface UserInterface {

  id: number;
  firstName: string;
  lastName: string;
  invitationUserId: number;
  email: string;
  phone: string;
  birthday: string;
  ikoint: number;
  gender: number;
  status: number;
  createdAt: string;
  updatedAt: string;
  roles: string[];
  invitationCodes: string[];
  // userRolePointOfSales: userRolePointOfSaleInterface[];

}
