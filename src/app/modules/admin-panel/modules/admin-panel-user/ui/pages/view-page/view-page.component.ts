import {Component, forwardRef, Inject, OnInit} from '@angular/core';
import {AdminPanelService} from '../../../../../shared/services/admin-panel.service';
import {UserService} from '../../../shared/services/user.service';
import {UserModel} from '../../../shared/models/user.model';
import {isNullOrUndefined} from "util";
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-view-page',
  templateUrl: './view-page.component.html',
  styleUrls: ['./view-page.component.scss']
})
export class ViewPageComponent implements OnInit {

  id = 'user-view';
  pageId = 'app-admin-user-view-view';

  loading = true;
  public paramID: string;

  constructor(
    public activatedRoute: ActivatedRoute,
    @Inject(forwardRef(() => AdminPanelService)) adminService,
    public userService: UserService,
  ) {
    adminService.titlePage = 'Перегляд користувача';
  }

  clickOnRefreshBtn() {

    if (isNullOrUndefined(this.paramID)) {
      return;
    }

    this.loader(true);
    this.userService.getUser(this.paramID).then(() => {

      this.loader(false);

    });

  }

  get user(): UserModel {

    return this.userService.user;

  }

  loader(run: boolean) {
    this.loading = run;
  }

  ngOnInit() {

    this.activatedRoute.parent.firstChild.params.subscribe(async (params) => {

      this.paramID = params.id;
      this.userService.getUser(this.paramID).then(() => {

        this.loader(false);

      });

    });

  }

}
