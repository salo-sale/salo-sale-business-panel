import {Component, forwardRef, Inject, Input, OnDestroy, OnInit} from '@angular/core';
import {AdminPanelService} from '../../../../../shared/services/admin-panel.service';
import {AppService} from '../../../../../../../shared/services/app.service';
import {UserService} from '../../../shared/services/user.service';
import {UserModel} from '../../../shared/models/user.model';
import {UserStatusEnum} from '../../../../../../../shared/enums/status/user.status.enum';
import {DeviceDetectorService} from 'ngx-device-detector';
import {isNullOrUndefined} from "util";

@Component({
  selector: 'app-admin-users-home',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit, OnDestroy {
  id = 'users';
  pageId = 'app-admin-users-home';

  public initialized = false;
  public initHideBody = true;
  public collapseContent = true;
  public loading = true;

  public adminService: AdminPanelService;

  @Input()
  companyId: number = null;

  isDesktop = false;

  constructor(
    @Inject(forwardRef(() => AdminPanelService)) adminService,
    public userService: UserService,
    public appService: AppService,
    private deviceService: DeviceDetectorService
  ) {


    this.isDesktop = deviceService.isDesktop();
    adminService.titlePage = 'Підписники';
    this.adminService = adminService;

  }


  clickOnRefreshBtn() {

    this.doFiltering();

  }

  clickOnCollapseBtn() {

    this.collapseContent = !this.collapseContent;

    if (this.filter.total === 0 && this.collapseContent) {

      this.doFiltering();

    }

  }

  private init() {

    if (!this.initialized) {

      this.initialized = true;


    }

  }

  loader(run: boolean) {
    this.loading = run;
  }

  clearSearch() {

    this.userService.filter.search = '';
    this.doFiltering();

  }

  get filter() {

    return this.userService.filter;

  }

  get users(): UserModel[] {

    return this.userService.users;

  }

  getStatusName(status) {

    return UserStatusEnum[status];

  }

  doFiltering(initPageList: boolean = true) {

    this.loader(true);



    if (initPageList) {

      this.userService.filter.resetPage();

    }

    this.userService.getUsers(true).then(() => {

      if (initPageList) {

        this.userService.filter.initPageList();

      }

      this.loader(false);

    });

  }

  ngOnInit() {

    if (isNullOrUndefined(this.companyId)) {
      this.adminService.titlePage = 'Підписники';
      this.initHideBody = false;
    } else {
      this.initHideBody = true;
    }
    this.collapseContent = !this.initHideBody;


    this.init();

    if (this.initHideBody) {

      this.loader(false);

    } else {

      this.doFiltering();

    }

  }

  ngOnDestroy(): void {

    this.userService.init();

  }

}
