import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomePageComponent} from './ui/pages/home-page/home-page.component';
import {AdminPanelUserComponent} from './admin-panel-user.component';
import {ViewPageComponent} from './ui/pages/view-page/view-page.component';

export const routes: Routes = [{
  path: '',
  component: AdminPanelUserComponent,
  children: [{
    path: '',
    component: HomePageComponent
  }, {
    path: 'view/:id',
    component: ViewPageComponent
  }]
}];


@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class AdminPanelUserRoutingModule { }
