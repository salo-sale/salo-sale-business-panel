import {Component, forwardRef, Inject, OnInit} from '@angular/core';
import {AdminPanelService} from '../../../../../shared/services/admin-panel.service';
import {AdvertisingBannerService} from '../../../shared/services/advertising-banner.service';
import {isNullOrUndefined} from 'util';
import {ActivatedRoute} from '@angular/router';
import {AppService} from '../../../../../../../shared/services/app.service';
import {PointOfSaleStatusEnum} from '../../../../../../../shared/enums/status/point-of-sale.status.enum';
import {RecommendedOfferModel} from '../../../shared/models/recommended-offer.model';
import {environment} from '../../../../../../../../environments/environment';
import {RecommendedOfferStatusEnum} from '../../../../../../../shared/enums/status/recommended-offer.status.enum';

@Component({
  selector: 'app-view-page',
  templateUrl: './view-page.component.html',
  styleUrls: ['./view-page.component.scss']
})
export class ViewPageComponent implements OnInit {

  constructor(
    @Inject(forwardRef(() => AdminPanelService)) adminService,
    public advertisingBannerService: AdvertisingBannerService,
    public activatedRoute: ActivatedRoute,
    public appService: AppService,
  ) {
    adminService.titlePage = 'Перегляд торгової точки';
  }

  get ab(): RecommendedOfferModel {

    return this.advertisingBannerService.document;

  }

  getCityName(localityId: number) {
    if (isNullOrUndefined(this.appService.cities)) {

      return 'Not found';

    } else {

      const object = this.appService.cities.find((city) => city.id === localityId);
      return  isNullOrUndefined(object) ? 'Not found' : object.name;

    }

  }

  tagName(id) {

    return this.appService.listsOfTags.find((list) => list.name === environment.defaultDate.objects.company).tags.find((tag) => tag.id === Number(id)).name;

  }

  statusName(status) {

    return RecommendedOfferStatusEnum[status];

  }

  ngOnInit() {

    this.activatedRoute.parent.firstChild.params.subscribe(async (params) => {

      if (isNullOrUndefined(this.ab)) {

        this.advertisingBannerService.getDocument(Number(params.id));

      }

    });

  }

  languageName(code) {

    return this.appService.languages.find((language) => language.code === code).name;

  }

}
