import {Component, forwardRef, Inject, OnInit} from '@angular/core';
import {AdminPanelService} from '../../../../../shared/services/admin-panel.service';

@Component({
  selector: 'app-create-page',
  templateUrl: './create-page.component.html',
  styleUrls: ['./create-page.component.scss']
})
export class CreatePageComponent implements OnInit {

  constructor(
    @Inject(forwardRef(() => AdminPanelService)) adminService,
  ) {
    adminService.titlePage = 'Нова рекомендація';
  }

  ngOnInit() {
  }

}
