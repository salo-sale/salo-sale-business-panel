import {Component, forwardRef, Inject, Input, OnDestroy, OnInit} from '@angular/core';
import {AdvertisingBannerService} from '../../../shared/services/advertising-banner.service';
import {AdminPanelService} from '../../../../../shared/services/admin-panel.service';
import {AppService} from '../../../../../../../shared/services/app.service';
import {OfferService} from '../../../../admin-panel-offer/shared/services/offer.service';
import {CityStatusEnum} from '../../../../../../../shared/enums/status/city.status.enum';
import {PointOfSaleStatusEnum} from '../../../../../../../shared/enums/status/point-of-sale.status.enum';
import {isNullOrUndefined} from 'util';
import {RecommendedOfferModel} from '../../../shared/models/recommended-offer.model';
import {environment} from '../../../../../../../../environments/environment';
import {RecommendedOfferStatusEnum} from '../../../../../../../shared/enums/status/recommended-offer.status.enum';
import {DeviceDetectorService} from 'ngx-device-detector';

@Component({
  selector: 'app-admin-panel-advertising-banner-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit, OnDestroy {
  id = 'recommended-offers';
  pageId = 'app-admin-recommended-offers-home';

  public initialized = false;
  public initHideBody = true;
  public collapseContent = true;
  public loading = true;

  isDesktop = false;
  public adminService: AdminPanelService;
  public openLoaderForIndex: number;

  @Input()
  companyId: number = null;
  @Input()
  offerId: number = null;

  public model: RecommendedOfferModel;

  constructor(
    @Inject(forwardRef(() => AdminPanelService)) adminService,
    public advertisingBannerService: AdvertisingBannerService,
    public offerService: OfferService,
    public appService: AppService,
    private deviceService: DeviceDetectorService
  ) {

    this.isDesktop = deviceService.isDesktop();
    this.model = new RecommendedOfferModel();
    this.openLoaderForIndex = null;
    this.adminService = adminService;

  }

  get filter() {

    return this.advertisingBannerService.filter;

  }

  get advertisingBanners(): RecommendedOfferModel[] {

    return this.advertisingBannerService.advertisingBanners;

  }


  clickOnRefreshBtn() {

    this.doFiltering();

  }

  clickOnCollapseBtn() {

    this.collapseContent = !this.collapseContent;

    console.log(this.filter.total);

    if (this.filter.total === 0 && this.collapseContent) {

      this.doFiltering();

    }

  }

  doFiltering(initPageList: boolean = true) {

    this.loader(true);

    if (initPageList) {

      this.advertisingBannerService.filter.resetPage();

    }

    //
    this.advertisingBannerService.getDocumentList(true, this.companyId, this.offerId).then(() => {

      if (initPageList) {

        this.advertisingBannerService.filter.initPageList();

      }

      this.loader(false);


    });

  }

  ngOnInit() {


    if (isNullOrUndefined(this.companyId) && isNullOrUndefined(this.offerId)) {

      this.initHideBody = false;
      this.adminService.titlePage = 'Рекомендації';

    } else {

      this.initHideBody = true;

    }

    this.collapseContent = !this.initHideBody;

    this.init();

    if (this.initHideBody) {

      this.loader(false);

    } else {

      this.doFiltering();

    }

  }

  selectPOS(pos: RecommendedOfferModel) {

    this.advertisingBannerService.document = pos;

  }

  getCityName(localityId: number) {
    if (isNullOrUndefined(this.appService.cities)) {

      return 'Not found';

    } else {

      const object = this.appService.cities.find((city) => city.id === localityId);
      return  isNullOrUndefined(object) ? 'Not found' : object.name;

    }

  }

  statusName(status) {

    return PointOfSaleStatusEnum[status];

  }

  sendCreateRecommendedOffer() {

    this.model.offerId = this.offerId;
    this.model.recommendedOfferLocalityList = this.offerService.document.offerLocalityList;
    this.model.status = 1;

    this.advertisingBannerService.initRecommendedOffer(this.model).then(async () => {

      await this.advertisingBannerService.getDocumentList(true, this.companyId, this.offerId);

    });

  }

  toggleStatusRecommendedOffer(recommendedOfferModel: RecommendedOfferModel) {
    console.log(recommendedOfferModel);

    recommendedOfferModel.status = recommendedOfferModel.status === 1 ? 2 : 1;


    this.advertisingBannerService.initRecommendedOffer(recommendedOfferModel).then(() => {

    });

  }

  deleteRecommendedOffer(ab: RecommendedOfferModel) {

    if (confirm('Впевнений що хочеш видалити цю рекомендацію?')) {

      this.advertisingBannerService.deleteDocument(ab.id).then(async (result) => {

        this.loader(true);

        this.advertisingBannerService.getDocumentList(true, this.companyId, this.offerId).then(() => {

          this.advertisingBannerService.filter.initPageList();
          this.loader(false);

        });

      });

    }

  }

  getStatus(status: number) {
    return RecommendedOfferStatusEnum[status];
  }

  private init() {

    if (!this.initialized) {

      this.initialized = true;


    }

  }

  loader(run: boolean) {
    this.loading = run;
  }

  ngOnDestroy(): void {

    this.advertisingBannerService.init();

  }
}
