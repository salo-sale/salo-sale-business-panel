import { Injectable } from '@angular/core';
import {AppService} from '../../../../../../shared/services/app.service';
import {RequestConfigurationModel} from '../../../../../../shared/models/request-configuration.model';
import {isNull, isNullOrUndefined} from 'util';
import {FilterModel} from '../models/filter.model';
import {NotificationStyle} from '../../../../shared/modules/notification/shared/enums/notification-style.enum';
import {NotificationService} from '../../../../shared/modules/notification/shared/services/notification.service';
import {RecommendedOfferModel} from '../models/recommended-offer.model';
import {RecommendedOfferInterface} from '../models/interfaces/recommended-offer.interface';

@Injectable({
  providedIn: 'root'
})
export class AdvertisingBannerService {

  public filter: FilterModel;
  public advertisingBanners: RecommendedOfferModel[];
  private initialize: boolean;
  document: RecommendedOfferModel;

  constructor(
    public appService: AppService,
    public notificationService: NotificationService
  ) {

    this.init();

  }

  async getDocumentList(reset: boolean = false, companyId: number = null, offerId: number = null): Promise<RecommendedOfferModel[]> {

    return new Promise<RecommendedOfferModel[]>(async (resolve, reject) => {

      if (this.advertisingBanners.length === 0 || reset) {

        const filter = this.filter.serialize();

        delete filter['total'];
        delete filter['pageList'];

        if (!isNull(companyId)) {

          filter.companyId = companyId;

        }

        if (!isNull(offerId)) {

          filter.offerId = offerId;

        }

        const requestConfiguration = new RequestConfigurationModel().deserialize({
          path: 'business',
          url: 'advertising-banners',
          params: {
            options: filter,
          }
        });

        await this.appService.authService.apiService.request(requestConfiguration).subscribe((data: {
          models: RecommendedOfferInterface[],
          total: number
        }) => {

          this.advertisingBanners = [];

          this.advertisingBanners = data.models.map((company) => new RecommendedOfferModel().deserialize(company));
          this.filter.total = data.total;

          // LocalStorageTool.setCompanies(this.pointsOfSale);
          // LocalStorageTool.setFilterCompany(this.filter);

          resolve(this.advertisingBanners);

        });

      } else {

        resolve(this.advertisingBanners);

      }

    });

  }

  init() {

    this.filter = new FilterModel();
    this.advertisingBanners = [];
    this.document = null;

    if (!this.initialize) {

      // this.pointsOfSale = LocalStorageTool.getCompanies();
      //
      // this.filter = LocalStorageTool.getFilterCompany();
      this.filter.initPageList();

      this.initialize = true;

    }

  }

  /**
   *
   * @param model
   */
  async save(model: RecommendedOfferModel): Promise<number> {

    return new Promise(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'business',
        url: 'advertising-banners',
        subUrl: model._isNew ? '' : '/' + model.id,
        params: {
          type: model._isNew ? 'post' : 'put',
          body: model.serialize()
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((id: number) => {

        console.log(id);

        this.notificationService.addToStream({
          title: 'Рекомендований банер',
          body: 'Успішно записано.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve(id);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Рекомендований банер',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(0);

      });

    });

  }

  /**
   *
   * @param id
   */
  async getDocument(id: number): Promise<RecommendedOfferModel> {

    return new Promise(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'business',
        url: 'advertising-banner',
        subUrl: id.toString()
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((company: RecommendedOfferInterface) => {

        console.log(company);
        this.document = new RecommendedOfferModel().deserialize(company);

        resolve(this.document);

      });

    });

  }

  /**
   *
   * @param model
   */
  async initRecommendedOffer(model: RecommendedOfferModel): Promise<number> {

    return new Promise(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'business',
        url: 'advertising-banner',
        subUrl: model.offerId.toString() + '/init-recommended-offer',
        params: {
          type: 'post',
          body: model.serialize()
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((id: number) => {

        console.log(id);

        this.notificationService.addToStream({
          title: 'Рекомендація',
          body: 'Успішно записано.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve(id);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Пропозиція',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(0);

      });

    });

  }

  /**
   *
   * @param id
   */
  async deleteDocument(id: number): Promise<number> {

    return new Promise<number>(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'business',
        url: 'advertising-banner',
        subUrl: id.toString(),
        params: {
          type: 'delete',
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((result) => {

        console.log(result);
        this.notificationService.addToStream({
          title: 'Рекомендація',
          body: 'Успішно видалено.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve(Number(result));

      });

    });

  }
}
