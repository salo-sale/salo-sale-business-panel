import { TestBed } from '@angular/core/testing';
import {AdvertisingBannerService} from './advertising-banner.service';


describe('AdvertisingBannerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdvertisingBannerService = TestBed.get(AdvertisingBannerService);
    expect(service).toBeTruthy();
  });
});
