import {OfferModel} from '../../../../admin-panel-offer/shared/models/offer.model';
import {OfferLocalityInterface} from '../../../../admin-panel-offer/shared/models/interfaces/offer-locality.interface';
import {RecommendedOfferLocalityInterface} from './recommended-offer-locality.interface';

export interface RecommendedOfferInterface {

  createdAt: string;
  id: number;
  recommendedOfferLocalityList: RecommendedOfferLocalityInterface[];
  position: number;
  status: number;
  offerId: number;
  updatedAt: string;
  numberOfClicks: number;
  offer?: OfferModel;

}
