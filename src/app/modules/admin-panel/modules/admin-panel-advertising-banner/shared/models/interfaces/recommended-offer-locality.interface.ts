export interface RecommendedOfferLocalityInterface {

  id?: number;
  recommendedOfferId?: number;
  localityId: number;
  without: number;

}
