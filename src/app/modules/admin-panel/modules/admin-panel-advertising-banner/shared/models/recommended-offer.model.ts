import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {isNullOrUndefined} from 'util';
import {RecommendedOfferInterface} from './interfaces/recommended-offer.interface';
import {DeserializableInterface} from '../../../../../../shared/models/interfaces/deserializable.interface';
import {SerializableInterface} from '../../../../../../shared/models/interfaces/serializable.interface';
import {OfferModel} from '../../../admin-panel-offer/shared/models/offer.model';
import {OfferLocalityInterface} from '../../../admin-panel-offer/shared/models/interfaces/offer-locality.interface';
import {RecommendedOfferLocalityInterface} from './interfaces/recommended-offer-locality.interface';

export class RecommendedOfferModel implements RecommendedOfferInterface, DeserializableInterface<RecommendedOfferInterface>, SerializableInterface<RecommendedOfferInterface> {

  /**
   *
   * Declaration variables
   *
   */

  private formBuilder: FormGroup;

  createdAt: string;
  id: number;
  recommendedOfferLocalityList: RecommendedOfferLocalityInterface[];
  position: number;
  status: number;
  numberOfClicks: number;
  offerId: number;
  updatedAt: string;
  offer?: OfferModel;

  _isNew: boolean;

  constructor() {

    this._isNew = true;
    this.initForm();

  }

  deserialize(input: RecommendedOfferInterface): this {

    this._isNew = false;

    return Object.assign(this, input);

  }

  serialize(): RecommendedOfferInterface {

    const object = Object.assign({} as RecommendedOfferInterface, this);
    delete object._isNew;
    delete object.formBuilder;
    delete object.createdAt;
    delete object.updatedAt;
    delete object.numberOfClicks;

    return object;

  }

  get formGroup(): FormGroup {

    return this.formBuilder;

  }

  get validForm(): boolean {

    if (this.formGroup.valid) {

      Object.keys(this.formGroup.value).forEach(key => {

        if (!isNullOrUndefined(this.formGroup.value[key])) {

         this[key] = this.formGroup.value[key];

        }

      });

      return true;

    } else {

      return false;

    }

  }

  public setOffer(offer: OfferModel = null) {

    console.assert(offer !== null);

    this.offer = offer;
    this.recommendedOfferLocalityList = this.offer.offerLocalityList;

  }

  public initForm(): void {

    this.formBuilder = new FormBuilder().group({
      status: [
        this.status,
      ],
      position: [
        this.position,
      ],
    });

  }

}
