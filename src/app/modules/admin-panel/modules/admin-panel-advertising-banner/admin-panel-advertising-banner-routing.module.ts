import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomePageComponent} from './ui/pages/home-page/home-page.component';
import {AdminPanelAdvertisingBannerComponent} from './admin-panel-advertising-banner.component';
import {ViewPageComponent} from './ui/pages/view-page/view-page.component';
import {CreatePageComponent} from './ui/pages/create-page/create-page.component';
import {UpdatePageComponent} from './ui/pages/update-page/update-page.component';

export const routes: Routes = [{
  path: '',
  component: AdminPanelAdvertisingBannerComponent,
  children: [{
    path: '',
    component: HomePageComponent
  }, {
    path: 'view/:id',
    component: ViewPageComponent
  }, {
    path: 'create/:offerId',
    component: CreatePageComponent
  }, {
    path: 'update/:id',
    component: UpdatePageComponent
  }]
}];


@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class AdminPanelAdvertisingBannerRoutingModule { }
