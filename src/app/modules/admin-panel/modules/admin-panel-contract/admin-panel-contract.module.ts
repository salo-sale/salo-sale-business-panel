import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminPanelContractComponent } from './admin-panel-contract.component';
import { HomePageComponent } from './ui/pages/home-page/home-page.component';
import {AdminPanelContractRoutingModule} from './admin-panel-contract-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CreatePageComponent } from './ui/pages/create-page/create-page.component';
import { FormComponent } from './ui/components/form/form.component';
import { UpdatePageComponent } from './ui/pages/update-page/update-page.component';
import { ViewPageComponent } from './ui/pages/view-page/view-page.component';
import {NgSelectModule} from '@ng-select/ng-select';
import {ImageCropperModule} from 'ngx-image-cropper';
import {ContractService} from './shared/services/contract.service';
import {NgbDatepickerModule} from '@ng-bootstrap/ng-bootstrap';
import {KarbaCoreModule} from '../../shared/modules/karba-core/karba-core.module';

@NgModule({
  declarations: [
    AdminPanelContractComponent,
    HomePageComponent,
    CreatePageComponent,
    FormComponent,
    UpdatePageComponent,
    ViewPageComponent
  ],
  providers: [
    ContractService
  ],
  exports: [
    HomePageComponent
  ],
    imports: [
        CommonModule,
        AdminPanelContractRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NgSelectModule,
        ImageCropperModule,
        NgbDatepickerModule,
        KarbaCoreModule
    ]
})
export class AdminPanelContractModule {

  constructor(
    private contractService: ContractService
  ) {
    contractService.init();
  }

}
