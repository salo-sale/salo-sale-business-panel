export interface FilterContractInterface {

  page: number;
  count: number;
  orderBy: string;
  sort: string;
  status: number;
  companyId: number;

}
