import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageComponent } from './ui/pages/home-page/home-page.component';
import {AdminPanelNotificationRoutingModule} from './admin-panel-notification-routing.module';
import {AdminPanelNotificationComponent} from './admin-panel-notification.component';
import {ViewPageComponent} from './ui/pages/view-page/view-page.component';
import {UpdatePageComponent} from './ui/pages/update-page/update-page.component';
import {CreatePageComponent} from './ui/pages/create-page/create-page.component';
import {FormComponent} from './ui/components/form/form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgSelectModule} from '@ng-select/ng-select';
import {AdminPanelPointOfSaleModule} from '../admin-panel-point-of-sale/admin-panel-point-of-sale.module';
import {ColorPickerModule} from 'ngx-color-picker';
import {NgbTimepickerModule} from '@ng-bootstrap/ng-bootstrap';
import {KarbaCoreModule} from '../../shared/modules/karba-core/karba-core.module';

@NgModule({
  declarations: [
    AdminPanelNotificationComponent,
    FormComponent,
    CreatePageComponent,
    UpdatePageComponent,
    ViewPageComponent,
    HomePageComponent
  ],
    imports: [
        CommonModule,
        AdminPanelNotificationRoutingModule,
        ReactiveFormsModule,
        FormsModule,
        NgSelectModule,
        AdminPanelPointOfSaleModule,
        ColorPickerModule,
        NgbTimepickerModule,
        KarbaCoreModule
    ]
})
export class AdminPanelNotificationModule { }
