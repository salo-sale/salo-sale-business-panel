import {Component, forwardRef, Inject, OnInit} from '@angular/core';
import {AdminPanelService} from '../../../../../shared/services/admin-panel.service';

@Component({
  selector: 'app-update-page',
  templateUrl: './update-page.component.html',
  styleUrls: ['./update-page.component.scss']
})
export class UpdatePageComponent implements OnInit {

  constructor(
    @Inject(forwardRef(() => AdminPanelService)) adminService,
  ) {
    adminService.titlePage = 'Редагування повідомлення';
  }

  ngOnInit() {
  }

}
