import {Component, forwardRef, Inject, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AdminPanelService} from '../../../../../shared/services/admin-panel.service';
import {isNullOrUndefined} from 'util';
import {environment} from '../../../../../../../../environments/environment';
import {AppService} from '../../../../../../../shared/services/app.service';
import {CompanyStatusEnum} from '../../../../../../../shared/enums/status/company.status.enum';
import {AppAdminPanelNotificationService} from '../../../shared/services/app-admin-panel-notification.service';
import {NotificationModel} from '../../../shared/models/notification.model';
import {NotificationStatusEnum} from '../../../../../../../shared/enums/status/notification.status.enum';
import {NotificationService} from "../../../../../shared/modules/notification/shared/services/notification.service";
import {NotificationStyle} from "../../../../../shared/modules/notification/shared/enums/notification-style.enum";

@Component({
  selector: 'app-view-page',
  templateUrl: './view-page.component.html',
  styleUrls: ['./view-page.component.scss']
})
export class ViewPageComponent implements OnInit, OnDestroy {

  public paramID: string;

  constructor(
    @Inject(forwardRef(() => AdminPanelService)) adminService,
    private activatedRoute: ActivatedRoute,
    public appAdminPanelNotificationService: AppAdminPanelNotificationService,
    public appService: AppService,
    public notificationService: NotificationService
  ) {
    adminService.titlePage = 'Перегляд повідомлення';

  }


  get document(): NotificationModel {

    return this.appAdminPanelNotificationService.document;

  }
  getStatusName(status: number) {
    return NotificationStatusEnum[status];
  }

  ngOnInit() {

    this.activatedRoute.parent.firstChild.params.subscribe(async (params) => {

      this.paramID = params.id;
      if (isNullOrUndefined(this.document)) {

        this.appAdminPanelNotificationService.getDocument(this.paramID);

      }

    });

  }

  localitiesName(localityIdList) {
    if (isNullOrUndefined(this.appService.cities)) {

      return 'Not found';

    } else {

      console.log(localityIdList);
      console.log(this.appService.cities);

      return  localityIdList.map((localityId) => this.appService.cities.find((city) => Number(city.id) === Number(localityId)).name);

    }

  }

  languageName(code) {

    return this.appService.languages.find((language) => language.code === code).name;

  }

  ngOnDestroy(): void {

    this.appAdminPanelNotificationService.document = null;

  }

  sendPushNotification() {
    if (confirm('Чи напевно хочеш відправити повідомлення на пристрої такі як: телефон, планшети, сайт і інші?') === true) {
      this.appAdminPanelNotificationService.sendPushNotification(this.paramID).then((success) => {

        if (success) {

          this.notificationService.addToStream({
            title: 'Повідомлення',
            body: 'Успішно відправлено.',
            show: true,
            style: NotificationStyle.SUCCESS
          });

        } else {

          this.notificationService.addToStream({
            title: 'Повідомлення',
            body: 'Не відправлено.',
            show: true,
            style: NotificationStyle.ERROR
          });

        }

      });
    }
  }

}
