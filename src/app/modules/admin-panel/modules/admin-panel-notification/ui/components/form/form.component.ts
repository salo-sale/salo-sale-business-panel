import { Component, OnInit } from '@angular/core';
import {environment} from '../../../../../../../../environments/environment';
import {ActivatedRoute, Route, Router} from '@angular/router';
import {isBoolean, isNullOrUndefined, isNumber} from 'util';
import {PointOfSaleModel} from '../../../../admin-panel-point-of-sale/shared/models/point-of-sale.model';
import {PointOfSaleService} from '../../../../admin-panel-point-of-sale/shared/services/point-of-sale.service';
import {AppService} from '../../../../../../../shared/services/app.service';
import {NotificationModel} from '../../../shared/models/notification.model';
import {AppAdminPanelNotificationService} from '../../../shared/services/app-admin-panel-notification.service';
import {CityModel} from '../../../../../../../shared/models/city.model';

@Component({
  selector: 'app-admin-panel-company-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  model: NotificationModel;
  pointsOfSale: PointOfSaleModel[];

  public loading: boolean;
  selectedTopics: any;
  topics: {name: string}[];
  public companyId: number;
  time: any;
  submitted: boolean;

  constructor(
    public activatedRoute: ActivatedRoute,
    public pointOfSaleService: PointOfSaleService,
    public appAdminPanelNotificationService: AppAdminPanelNotificationService,
    public appService: AppService,
    public router: Router
  ) {

    this.topics = environment.topics;
    this.loading = false;
    this.model = null;
    this.pointsOfSale = [];
    this.submitted = false;

  }

  get cities(): CityModel[] {

    // TODO, Return all types!
    return this.appService.cities;

  }

  ngOnInit() {

    this.activatedRoute.parent.firstChild.params.subscribe(async (params) => {

      if (!isNullOrUndefined(Number(params.id)) && !isNaN(Number(params.id))) {

        if (isNullOrUndefined(this.appAdminPanelNotificationService.document)) {

          await this.appAdminPanelNotificationService.getDocument(params.id);

        }

        this.model = this.appAdminPanelNotificationService.document;

      } else {

        this.model = new NotificationModel();

      }

      if (isNullOrUndefined(params.companyId)) {

        this.selectedTopics = ['client_web', 'client_ios', 'client_android'];

      } else {

        this.companyId = Number(params.companyId);
        this.model.access = 2;
        this.selectedTopics = ['company_' + params.companyId];

      }

      await this.model.initForm();

    });

  }

  changed(data) {

    console.log(data);

    // this.model.tags = data.value.map(value => parseInt(value));

  }

  async saveDocument() {

    this.submitted = true;

    if (this.model.validForm) {

      let canSave: boolean | number = true;

      if (canSave) {

        console.log(this.model.serialize());
        canSave = await this.appAdminPanelNotificationService.saveDocument(this.model);

        if (canSave > 0) {

          this.submitted = false;
          this.router.navigateByUrl('business/notification/view/' + canSave);

        }

      }

    } else {

      console.warn(this.model.formGroup);
      console.warn(this.model);

    }

  }

  addNewPointOfSaleToList() {

    const model = new PointOfSaleModel();
    model.initForm();
    this.pointsOfSale.push(model);

  }

  removeItem(index: number) {

    console.log(index);
    this.pointsOfSale.splice(index, 1);

  }

  addTagPromise(name) {
    return new Promise((resolve) => {
      this.loading = true;
      // Simulate backend call.
      resolve(name);
      this.loading = false;
    });
  }

  changeSearch($event) {
    console.log($event);
  }

}
