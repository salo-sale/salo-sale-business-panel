import {NotificationInterface} from './interfaces/notification.interface';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {DeserializableInterface} from '../../../../../../shared/models/interfaces/deserializable.interface';
import {SerializableInterface} from '../../../../../../shared/models/interfaces/serializable.interface';
import { isNullOrUndefined } from 'util';
import {NotificationLanguageModel} from './notification-language.model';
import {NotificationLanguageInterface} from './interfaces/notification-language.interface';
import {LanguageModel} from '../../../admin-panel-point-of-sale/shared/models/language.model';
import {LanguageInterface} from '../../../admin-panel-point-of-sale/shared/models/interfaces/languageInterface';


export class NotificationModel implements NotificationInterface, DeserializableInterface<NotificationInterface>, SerializableInterface<NotificationInterface> {

  /**
   *
   * Declaration variables
   *
   */

  private formBuilder: FormGroup;

  access: number;
  createdAt: string;
  id: number;
  languages: NotificationLanguageModel[];
  status: number;
  companyId: number;
  topics: string[];
  scheduledDate: string;
  localities: number[];
  updatedAt: string;

  _isNew: boolean;

  constructor() {

    this._isNew = true;
    this.status = 1;
    this.access = 1;
    this.initForm();

  }

  deserialize(input: NotificationInterface): this {

    this._isNew = false;

    const object = Object.assign(this, input);

    if (!isNullOrUndefined(object.languages) && object.languages.length > 0) {

      object.languages = object.languages.map((language) => {

        return new NotificationLanguageModel().deserialize(language);

      });

    }

    return object;

  }

  serialize(): NotificationInterface {

    const object = Object.assign({} as NotificationInterface, this);
    delete object._isNew;
    delete object.formBuilder;

    object.languages = object.languages.map((language: NotificationLanguageModel) => language.serialize()) as NotificationLanguageInterface[] & NotificationLanguageModel[];

    return object;

  }

  get formGroup(): FormGroup {

    return this.formBuilder;

  }

  get validForm(): boolean {

    if (this.formGroup.valid) {

      Object.keys(this.formGroup.value).forEach(key => {

        if (!isNullOrUndefined(this.formGroup.value[key])) {

          this[key] = this.formGroup.value[key];

        }

      });

      this.languages = this.formGroup.value.languages.map((l: NotificationLanguageInterface) => new NotificationLanguageModel().deserialize(l));

      return true;

    } else {

      return false;

    }

  }

  public initForm(): void {

    let languages = [
      new FormGroup({

        languageCode: new FormControl('uk', [Validators.required]),
        title: new FormControl(null, [Validators.required]),
        body: new FormControl(null, [Validators.required]),

      })
    ];

    if (!this._isNew) {

      languages = [...this.languages.map((language) => {

        return new FormGroup({

          id: new FormControl(language.id, [Validators.required]),
          languageCode: new FormControl(language.languageCode, [Validators.required]),
          title: new FormControl(language.title, [Validators.required]),
          body: new FormControl(language.body, [Validators.required]),

        });

      })];

    }

    this.formBuilder = new FormBuilder().group({
      access: [
        Number(this.access),
      ],
      topics: [
        this.topics,
      ],
      status: [
        this.status,
      ],
      scheduleDate: [
        this.scheduledDate,
      ],
      localities: [
        isNullOrUndefined(this.localities) ? [] : this.localities.map((localityId) => Number(localityId)),
      ],
      languages: new FormArray(languages, [Validators.required]),
    });

  }

  get formLanguages(): FormArray {
    return this.formGroup.get('languages') as FormArray;
  }

  addNewLanguageToForm() {

    this.formLanguages.push(new FormGroup({

      languageCode: new FormControl('uk', [Validators.required]),
      title: new FormControl('', [Validators.required]),
      body: new FormControl('', [Validators.required]),

    }));

  }

  deleteFormLanguage(index: number) {
    this.formLanguages.controls.splice(index, 1);
  }

}
