import {DeserializableInterface} from '../../../../../../shared/models/interfaces/deserializable.interface';
import {SerializableInterface} from '../../../../../../shared/models/interfaces/serializable.interface';
import {FormBuilder, FormGroup} from '@angular/forms';
import {NotificationLanguageInterface} from './interfaces/notification-language.interface';
import {isNullOrUndefined} from "util";

export class NotificationLanguageModel implements NotificationLanguageInterface, DeserializableInterface<NotificationLanguageInterface>, SerializableInterface<NotificationLanguageInterface> {


  /**
   *
   * Declaration variables
   *
   */

  private formBuilder: FormGroup;

  id: number;
  title: string;
  body: string;
  languageCode: string;
  notificationId: number;

  _isNew: boolean;

  constructor() {

    this._isNew = true;
    this.initForm();

  }

  deserialize(input: NotificationLanguageInterface): this {

    this._isNew = false;

    return Object.assign(this, input);

  }

  serialize(): NotificationLanguageInterface {

    const object = Object.assign({} as NotificationLanguageInterface, this);
    delete object._isNew;
    delete object.formBuilder;

    return object;

  }

  get formGroup(): FormGroup {

    return this.formBuilder;

  }

  get validForm(): boolean {

    if (this.formGroup.valid) {

      Object.keys(this.formGroup.value).forEach(key => {

        if (!isNullOrUndefined(this.formGroup.value[key])) {

          this[key] = this.formGroup.value[key];

        }

      });

      return true;

    } else {

      return false;

    }

  }

  public initForm(): void {

    this.formBuilder = new FormBuilder().group({
    });

  }

}
