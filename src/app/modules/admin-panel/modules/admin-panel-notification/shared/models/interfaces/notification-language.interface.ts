export interface NotificationLanguageInterface {

  id: number;
  title: string;
  body: string;
  languageCode: string;
  notificationId: number;

}
