import {NotificationLanguageInterface} from './notification-language.interface';

export interface NotificationInterface {

  id: number;
  topics: string[];
  access: number;
  status: number;
  companyId: number;
  createdAt: string;
  updatedAt: string;
  scheduledDate: string;
  languages: NotificationLanguageInterface[];

}
