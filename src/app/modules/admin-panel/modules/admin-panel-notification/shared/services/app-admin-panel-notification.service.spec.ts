import { TestBed } from '@angular/core/testing';
import {AppAdminPanelNotificationService} from './app-admin-panel-notification.service';


describe('AppAdminPanelNotificationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AppAdminPanelNotificationService = TestBed.get(AppAdminPanelNotificationService);
    expect(service).toBeTruthy();
  });
});
