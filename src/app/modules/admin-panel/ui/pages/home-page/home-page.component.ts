import {Component, forwardRef, Inject, OnInit} from '@angular/core';
import {AdminPanelService} from '../../../shared/services/admin-panel.service';
import {AppService} from '../../../../../shared/services/app.service';
import {DashboardModel} from '../../../shared/models/dashboard.model';
import {StatisticsModel} from '../../../shared/models/statistics.model';

@Component({
  selector: 'app-admin-panel-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  public adminService: AdminPanelService;

  constructor(
    @Inject(forwardRef(() => AdminPanelService)) adminService,
    public appService: AppService,
  ) {
    adminService.titlePage = 'Головна';
    this.adminService = adminService;
  }

  get statistics(): StatisticsModel[] {

    return this.adminService.statistics;

  }

  ngOnInit() {

    // document.querySelector('.loader').classList.remove('d-none');

    // this.adminService.getDashboard(true).then(() => {
    //
    //   document.querySelector('.loader').classList.add('d-none');
    //
    // });

  }

  typeName(type: string) {
    /**
     *
     'USERS': 'Підписники',
     'COMPANIES': 'Бізнеси',
     'POINTS_OF_SALE': 'Торгові точки',
     'OFFERS': 'Пропозиції',
     'ADVERTISING_BANNERS': 'Рекомендацій',
     'CONTRACTS': 'Контрактів',
     'AVG_OFFERS_VIEWS': 'Середня кількість переглядів пропозицій',
     */
    return {
      1: 'Підписники',
      2: 'Бізнеси',
      3: 'Торгові точки',
      4: 'Пропозиції',
      5: 'Рекомендацій',
      6: 'Контрактів',
      7: 'Середня кількість переглядів пропозицій',
    }[type];
  }
}
