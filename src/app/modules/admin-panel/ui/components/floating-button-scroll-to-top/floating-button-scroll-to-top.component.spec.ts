import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FloatingButtonScrollToTopComponent } from './floating-button-scroll-to-top.component';

describe('FloatingButtonScrollToTopComponent', () => {
  let component: FloatingButtonScrollToTopComponent;
  let fixture: ComponentFixture<FloatingButtonScrollToTopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FloatingButtonScrollToTopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FloatingButtonScrollToTopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
