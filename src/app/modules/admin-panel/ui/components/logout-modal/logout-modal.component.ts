import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-panel-logout-modal',
  templateUrl: './logout-modal.component.html',
  styleUrls: ['./logout-modal.component.scss']
})
export class LogoutModalComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
