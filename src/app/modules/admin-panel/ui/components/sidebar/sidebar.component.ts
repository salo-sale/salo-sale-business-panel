import {ChangeDetectorRef, Component, forwardRef, Inject, OnDestroy, OnInit} from '@angular/core';
import {routes} from '../../../admin-panel-routing.module';
import {Routes} from '@angular/router';
import {AdminPanelService} from '../../../shared/services/admin-panel.service';
import * as $ from 'jquery';
import {Subscription} from 'rxjs';
import {AuthPanelService} from '../../../../auth-panel/shared/services/auth-panel.service';
import {DeviceDetectorService} from 'ngx-device-detector';
import {AuthService} from '../../../../../shared/services/auth.service';

@Component({
  selector: 'app-admin-panel-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit, OnDestroy {

  public routes: Routes;
  public adminService: AdminPanelService;
  public authPanelService: AuthPanelService;
  public showSidebar: boolean;
  private streamSidebar: Subscription;
  id = 'sidebar-component';

  constructor(
    @Inject(forwardRef(() => AdminPanelService)) adminService,
    @Inject(forwardRef(() => AuthPanelService)) authPanelService,
    private deviceService: DeviceDetectorService,
    public authService: AuthService,

    private changeDetectorRef: ChangeDetectorRef
  ) {

    this.adminService = adminService;
    this.authPanelService = authPanelService;
    this.routes = routes;

    this.streamSidebar = this.adminService.sidebarBehaviorSubject.subscribe((show) => {

      this.showSidebar = show;

      if (show) {

        // $('.sidebar .collapse').collapse('hide');
        document.body.classList.add('sidebar-toggled');

      } else {

        document.body.classList.remove('sidebar-toggled');

      }

    });

  }

  changeSelectedCompany($event) {

    this.authService.user.selectedCompany = this.authService.user.companies.find((a) => a.id === $event.id);

    this.authService.user.saveToLocalStorage();
    // location.replace('/#/business/');
    this.authService.redirect('/business');
    // TODO chanel for change company

  }

  ngOnInit() {

    if (!this.deviceService.isDesktop()) {

      this.adminService.toggleSidebar(false);

    }

  }

  ngOnDestroy(): void {

    this.streamSidebar.unsubscribe();

  }

}
