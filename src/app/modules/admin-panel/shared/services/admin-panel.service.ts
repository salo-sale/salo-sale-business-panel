import {Inject, Injectable, PLATFORM_ID} from '@angular/core';
import {Router} from '@angular/router';
import {BehaviorSubject} from 'rxjs';
import {isPlatformBrowser} from '@angular/common';
import {AuthService} from '../../../../shared/services/auth.service';
import {TagModel} from '../models/tag.model';
import {TagInterface} from '../models/interfaces/tag.interface';
import {RequestConfigurationModel} from '../../../../shared/models/request-configuration.model';
import {LocalStorageTool} from '../../../../shared/tools/local-storage.tool';
import {FilterTagModel} from '../models/filter-tag.model';
import {LocalityAdminModel} from '../models/locality-admin.model';
import {FilterCityModel} from '../models/filter-city.model';
import {CityInterface} from '../models/interfaces/city.interface';
import {DashboardModel} from '../models/dashboard.model';
import {DashboardInterface} from '../models/interfaces/dashboard.interface';
import {NotificationStyle} from '../modules/notification/shared/enums/notification-style.enum';
import {NotificationService} from '../modules/notification/shared/services/notification.service';
import {StatisticsModel} from '../models/statistics.model';
import {StatisticsInterface} from '../models/interfaces/statistics.interface';
import {isNullOrUndefined} from 'util';

@Injectable({
  providedIn: 'root'
})
export class AdminPanelService {

  public titlePage: string;
  public showSidebar = true;
  public sidebarBehaviorSubject: BehaviorSubject<boolean>;
  public tags: TagModel[];
  public cities: LocalityAdminModel[];
  public filterTag: FilterTagModel;
  public initialize: boolean;
  public filterCity: FilterCityModel;
  public dashboard: DashboardModel;
  public statistics: StatisticsModel[];

  /**
   *
   * @param router
   * @param authService
   * @param notificationService
   * @param platformId
   */
  constructor(
    public router: Router,
    public authService: AuthService,
    public notificationService: NotificationService,
    @Inject(PLATFORM_ID) private platformId
  ) {

    this.dashboard = null;
    this.statistics = [];

    this.initialize = false;

    this.showSidebar = true;

    // let fixedSidebar;
    //
    // fixedSidebar = JSON.parse(localStorage.getItem('fixedSidebar'));
    //
    // if (isNullOrUndefined(fixedSidebar)) {
    //
    //   fixedSidebar = true;
    //   localStorage.setItem('fixedSidebar', JSON.stringify(fixedSidebar));
    //
    // }

    // this.fixedSidebar = fixedSidebar;

    this.sidebarBehaviorSubject = new BehaviorSubject<boolean>(this.showSidebar);

  }

  /**
   *
   */
  public toggleSidebar(show = null) {

    this.showSidebar = isNullOrUndefined(show) ? !this.showSidebar : show;

    // console.log(this.fixedSidebar);

    // localStorage.setItem('fixedSidebar', JSON.stringify(this.fixedSidebar));

    this.sidebarBehaviorSubject.next(this.showSidebar);

  }

  async getDashboard(reset: boolean = false): Promise<StatisticsModel[]> {

    return new Promise<StatisticsModel[]>(async (resolve, reject) => {

      if (this.dashboard === null || reset) {

        const requestConfiguration = new RequestConfigurationModel().deserialize({
          path: 'business',
          url: 'dashboards',
        });

        await this.authService.apiService.request(requestConfiguration).subscribe((data: StatisticsInterface[]) => {

          this.statistics = [];
          this.statistics.push(...data.map(sta => new StatisticsModel().deserialize(sta)));
          // this.dashboard = null;
          //
          // this.dashboard = new DashboardModel().deserialize(data);
          console.log(this.statistics);

          // LocalStorageTool.setTags(this.tags);

          resolve(this.statistics);

        });

      } else {

        resolve(this.statistics);

      }

    });

  }

  async getTags(reset: boolean = false): Promise<TagModel[]> {

    return new Promise<TagModel[]>(async (resolve, reject) => {

      if (this.tags.length === 0 || reset) {

        const filter = this.filterTag.serialize();

        delete filter['total'];
        delete filter['pageList'];

        const requestConfiguration = new RequestConfigurationModel().deserialize({
          path: 'business',
          url: 'tags',
          params: {
            options: filter,
          }
        });

        await this.authService.apiService.request(requestConfiguration).subscribe((data: {
          models: TagInterface[],
          total: number
        }) => {

          this.tags = [];

          this.tags = data.models.map((tag) => new TagModel().deserialize(tag));
          this.filterTag.total = data.total;

          LocalStorageTool.setTags(this.tags);
          LocalStorageTool.setFilterTag(this.filterTag);

          resolve(this.tags);

        });

      } else {

        resolve(this.tags);

      }

    });

  }

  async getCities(reset: boolean = false): Promise<LocalityAdminModel[]> {

    return new Promise<LocalityAdminModel[]>(async (resolve, reject) => {

      if (this.cities.length === 0 || reset) {

        const filter = this.filterCity.serialize();

        delete filter['total'];
        delete filter['pageList'];

        const requestConfiguration = new RequestConfigurationModel().deserialize({
          path: 'business',
          url: 'cities',
          params: {
            options: filter,
          }
        });

        await this.authService.apiService.request(requestConfiguration).subscribe((data: {
          models: CityInterface[],
          total: number
        }) => {

          this.cities = [];

          if (data.models.length > 0) {

            this.cities = data.models.map((city) => new LocalityAdminModel().deserialize(city));

          }

          this.filterCity.total = data.total;

          resolve(this.cities);

        });

      } else {

        resolve(this.cities);

      }

    });

  }

  init() {

    if (!this.initialize) {

      this.tags = LocalStorageTool.getTags();

      this.filterTag = LocalStorageTool.getFilterTag();
      this.filterTag.initPageList();

      // this.cities = LocalStorageTool.getAdminCities();
      //
      // this.filterCity = LocalStorageTool.getAdminFilterCity();
      this.cities = [];
      this.filterCity = new FilterCityModel();
      this.filterCity.initModel();
      this.filterCity.initPageList();

      this.initialize = true;

    }

  }

  async saveTags(selectedTag: TagModel): Promise<number> {

    return new Promise<number>(async (resolve, reject) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'business',
        url: 'tag',
        params: {
          type: selectedTag._isNew ? 'post' : 'put',
          body: selectedTag.serialize()
        }
      });

      await this.authService.apiService.request(requestConfiguration).subscribe((id: number) => {

        console.log(id);


        this.notificationService.addToStream({
          title: 'Теги',
          body: 'Успішно записано.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve(id);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Теги',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(0);

      });

    });

  }

  async saveLocality(selectedCity: LocalityAdminModel): Promise<number> {

    return new Promise<number>(async (resolve, reject) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'business',
        url: selectedCity._isNew ? 'cities' : 'city',
        subUrl: selectedCity._isNew ? '' : selectedCity.id.toString(),
        params: {
          type: selectedCity._isNew ? 'post' : 'put',
          body: selectedCity.serialize()
        }
      });

      await this.authService.apiService.request(requestConfiguration).subscribe((id: number) => {

        console.log(id);


        this.notificationService.addToStream({
          title: 'Локалізація',
          body: 'Успішно записано.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve(id);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Місто',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(0);

      });

    });

  }

  async deleteLocality(selectedCity: LocalityAdminModel): Promise<number> {

    return new Promise<number>(async (resolve, reject) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'business',
        url: 'city',
        subUrl: selectedCity.id.toString(),
        params: {
          type: 'delete',
          body: selectedCity.serialize()
        }
      });

      await this.authService.apiService.request(requestConfiguration).subscribe((id: number) => {

        console.log(id);


        this.notificationService.addToStream({
          title: 'Локалізація',
          body: 'Успішно видалили.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve(id);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Місто',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(0);

      });

    });

  }

  async toggleTagToTagList(tag: TagModel, isDelete: boolean, name: string): Promise<boolean> {

    return new Promise<boolean>(async (resolve, reject) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'business',
        url: 'tag-to-list-of-tags',
        params: {
          type: 'post',
          body: {
            tagId: tag.id,
            listName: name,
            isDelete: isDelete
          },
        }
      });

      await this.authService.apiService.request(requestConfiguration).subscribe((result: boolean) => {

        console.log(result);

        this.notificationService.addToStream({
          title: 'Тег',
          body: 'Операція успішно виконана.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve(true);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Тег',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(false);

      });

    });

  }

}
