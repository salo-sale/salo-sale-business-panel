import { Pipe, PipeTransform } from '@angular/core';
import {isNullOrUndefined} from "util";

@Pipe({
  name: 'pipeIsUndefinedOrNull'
})
export class PipeIsUndefinedOrNullPipe implements PipeTransform {

  transform(value: any, args?: any): any {

    if (isNullOrUndefined(value) || value.length === 0) {
      return 'No data';
    }

    if (isNullOrUndefined(args) || args.length === 0) {
      return value;
    }

    if (isNullOrUndefined(value[args]) || value[args].length === 0) {
      return 'No data';
    }

    return value[args];
  }

}
