import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PipeIsUndefinedOrNullPipe } from './shared/pipes/pipe-is-undefined-or-null.pipe';

@NgModule({
  declarations: [PipeIsUndefinedOrNullPipe],
  exports: [
    PipeIsUndefinedOrNullPipe
  ],
  imports: [
    CommonModule
  ]
})
export class PipeIsUndefinedOrNullModule { }
