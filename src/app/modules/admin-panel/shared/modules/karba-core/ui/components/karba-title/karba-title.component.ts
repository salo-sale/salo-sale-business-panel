import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {isNullOrUndefined} from 'util';

@Component({
  selector: 'app-karba-title',
  templateUrl: './karba-title.component.html',
  styleUrls: ['./karba-title.component.scss']
})
export class KarbaTitleComponent implements OnInit {

  @Input() coreId: string;
  @Input() pageId: string;

  @Input() showRefresh = true;
  @Input() showTitle = true;
  @Input() showTotal = true;
  @Input() showSubTotal = false;
  @Input() hideRightBlock = false;

  @Input() loading = true;

  @Input() title: string;
  @Input() initHideBody = false;

  @Input() titleLikeLink = false;

  @Input() total: any;
  @Input() subTotal: any;
  @Input() customRouterLink: [] = [];

  @Input() refreshBtnId;
  @Input() collapseBtnId;
  @Input() collapseBtnOpenId;

  @Input() colMdTitle = 5;

  @Output() clickOnRefreshBtn = new EventEmitter();
  @Output() clickOnCollapseBtn = new EventEmitter();

  _collapse = true;

  constructor(
  ) { }

  ngOnInit() {

    if (isNullOrUndefined(this.refreshBtnId)) {

      this.refreshBtnId = 'btn-refresh-content-' + this.coreId;

    }

    if (isNullOrUndefined(this.collapseBtnId)) {

      this.collapseBtnId = 'btn-collapse-' + this.coreId;

    }

    if (isNullOrUndefined(this.collapseBtnOpenId)) {

      this.collapseBtnOpenId = 'card-body-' + this.coreId;

    }

    this._collapse = !this.initHideBody;

  }

  _clickOnRefreshBtn() {

    this.clickOnRefreshBtn.emit();

  }

  _clickOnCollapseBtn() {

    this._collapse = !this._collapse;
    this.clickOnCollapseBtn.emit();

  }

}
