import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ListPageComponent} from './ul/components/list-page/list-page.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {KarbaCoreModule} from '../karba-core/karba-core.module';

@NgModule({
  declarations: [
    ListPageComponent
  ],
  exports: [
    ListPageComponent
  ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        KarbaCoreModule
    ]
})
export class LocalityListModule { }
