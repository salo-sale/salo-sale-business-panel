import {Component, forwardRef, Inject, OnInit} from '@angular/core';
import {NotificationModel} from '../../../shared/models/notification.model';
import {NotificationService} from '../../../shared/services/notification.service';
import {NotificationStyle} from '../../../shared/enums/notification-style.enum';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

  public notificationList: NotificationModel[];

  public notificationService: NotificationService;

  constructor(
    @Inject(forwardRef(() => NotificationService)) notificationService,
  ) {

    this.notificationService = notificationService;

    this.notificationService.stream.subscribe((notificationList) => {

      console.table(notificationList);

      this.notificationList = notificationList;

    });

  }


  ngOnInit() {

    // setTimeout(() => {
    //
    //   this.notificationService.addToStream({
    //     title: 'Firma',
    //     body: 'Zostało poprawnie zapisane',
    //     show: true,
    //     useTimer: false,
    //     style: NotificationStyle.SUCCESS
    //   });
    //
    //   setTimeout(() => {
    //
    //     this.notificationService.addToStream({
    //       title: 'Firma',
    //       body: 'Zostało poprawnie zapisane',
    //       show: true,
    //       useTimer: false,
    //       style: NotificationStyle.SUCCESS
    //     });
    //
    //     setTimeout(() => {
    //
    //       this.notificationService.addToStream({
    //         title: 'Firma',
    //         body: 'Zostało poprawnie zapisane',
    //         show: true,
    //         useTimer: false,
    //         style: NotificationStyle.SUCCESS
    //       });
    //
    //     }, 3000);
    //
    //   }, 3000);
    //
    // }, 3000);

  }

  close(index: number) {

    this.notificationService.close(index);

  }

}
