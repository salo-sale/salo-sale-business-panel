import {NotificationStyle} from '../../enums/notification-style.enum';

export interface NotificationInterface {

  _bgColor?: string;

  title: string;
  body: string;
  showCloseButton?: boolean;
  link?: string;
  useTimer?: boolean;
  timer?: number;
  style?: NotificationStyle;
  show: boolean;

}
