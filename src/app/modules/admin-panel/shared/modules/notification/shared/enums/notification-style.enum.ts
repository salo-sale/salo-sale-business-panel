export enum NotificationStyle {

  INFO,
  PRIMARY,
  SUCCESS,
  ERROR,
  WARNING

}
