import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {NotificationModel} from '../models/notification.model';
import {NotificationInterface} from '../models/interfaces/notification.interface';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  public readonly stream: BehaviorSubject<NotificationModel[]>;

  public notificationList: NotificationModel[];

  constructor() {

    this.notificationList = [];
    // this.notificationList = [new NotificationModel({body: 'hello', show: true, timer: 20000, title: 'world'})];
    this.stream = new BehaviorSubject<NotificationModel[]>(this.notificationList);

  }

  public addToStream(notification: NotificationInterface) {

    console.table(notification);

    this.notificationList.push(new NotificationModel().deserialize(notification));

    this.stream.next(this.notificationList);

  }

  close(index: number) {

    this.notificationList[index].show = false;

    setTimeout(() => {

      this.notificationList.splice(index, 1);

    }, 500);

    this.stream.next(this.notificationList);

  }

}
