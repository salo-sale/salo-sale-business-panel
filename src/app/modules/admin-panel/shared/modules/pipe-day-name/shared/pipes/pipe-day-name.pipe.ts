import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pipeDayName'
})
export class PipeDayNamePipe implements PipeTransform {

  transform(value: number, args?: any): any {

    return ['Неділя', 'Понеділок', 'Вівторок', 'Середа', 'Четвер', 'П`ятниця', 'Субота'][value];

  }

}
