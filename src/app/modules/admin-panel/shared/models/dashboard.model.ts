import {DashboardInterface} from './interfaces/dashboard.interface';
import {DeserializableInterface} from '../../../../shared/models/interfaces/deserializable.interface';
import {SerializableInterface} from '../../../../shared/models/interfaces/serializable.interface';

export class DashboardModel implements DashboardInterface, DeserializableInterface<DashboardInterface>, SerializableInterface<DashboardInterface> {

  advertisingBannerCount: number;
  companyCount: number;
  pointOfSaleCount: number;
  userCount: number;
  offerCount: number;
  contractCount: number;

  deserialize(input: DashboardInterface): this {

    return Object.assign(this, input);

  }

  serialize(): DashboardInterface {

    return Object.assign({} as DashboardInterface, this);

  }

}
