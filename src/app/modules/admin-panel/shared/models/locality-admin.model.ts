import {DeserializableInterface} from '../../../../shared/models/interfaces/deserializable.interface';
import {SerializableInterface} from '../../../../shared/models/interfaces/serializable.interface';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {TagLanguageModel} from './tag-language.model';
import {CityInterface} from './interfaces/city.interface';
import {LocalityLanguageModel} from './locality-language.model';
import {isNullOrUndefined} from 'util';
import {CityLanguageInterface} from './interfaces/city-language.interface';

export class LocalityAdminModel implements CityInterface, DeserializableInterface<CityInterface>, SerializableInterface<CityInterface> {

  /**
   *
   * Declaration variables
   *
   */

  private formBuilder: FormGroup;

  id: number;
  languages: LocalityLanguageModel[];
  languagesStrings: string[];
  type: string;
  status: number;

  zoom: number;
  longitude: number;
  latitude: number;
  countryId: number;
  regionId: number;
  cityId: number;

  idList: number[];

  _isNew: boolean;

  constructor() {

    this._isNew = true;
    this.idList = [];
    this.zoom = 15;
    this.status = 1;
    this.type = 'COUNTRY';
    this.initForm();

  }

  deserialize(input: CityInterface): this {

    this._isNew = false;

    const object = Object.assign(this, input);

    object.languagesStrings = object.languages.map((lan) => lan.name);
    object.languages = object.languages.map((name) => new LocalityLanguageModel().deserialize(name));

    object.idList = [object.id, object.cityId, object.regionId, object.countryId];
    object.idList = object.idList.filter((id) => id !== null);

    return object;

  }

  serialize(): CityInterface {

    const object = Object.assign({} as CityInterface, this);
    delete object._isNew;
    delete object.formBuilder;
    object.latitude = Number(object.latitude);
    object.longitude = Number(object.longitude);

    return object;

  }

  get formGroup(): FormGroup {

    return this.formBuilder;

  }

  get validForm(): boolean {

    if (this.formGroup.valid) {


      Object.keys(this.formGroup.value).forEach(key => {

        if (!isNullOrUndefined(this.formGroup.value[key])) {

          this[key] = this.formGroup.value[key];

        }

      });

      return true;

    } else {

      return false;

    }

  }

  public initForm(): void {

    // Todo get info about hoive many langauges

    let languages = [
      new FormGroup({

        languageCode: new FormControl('en', [Validators.required]),
        name: new FormControl('', [Validators.required]),

      }),
      new FormGroup({

        languageCode: new FormControl('ru', [Validators.required]),
        name: new FormControl('', [Validators.required]),

      }),
      new FormGroup({

        languageCode: new FormControl('uk', [Validators.required]),
        name: new FormControl('', [Validators.required]),

      })
    ];

    if (!this._isNew) {

      languages = [...this.languages.map((name) => {

        return new FormGroup({

          id: new FormControl(name.id, [Validators.required]),
          languageCode: new FormControl(name.languageCode, [Validators.required]),
          name: new FormControl(name.name, [Validators.required]),

        });

      })];

    }

    this.formBuilder = new FormBuilder().group({
      zoom: new FormControl(this.zoom),
      latitude: new FormControl(this.latitude, [Validators.required]),
      longitude: new FormControl(this.longitude, [Validators.required]),
      status: new FormControl(this.status),
      languages: new FormArray(languages, [Validators.required]),
    });

  }

}
