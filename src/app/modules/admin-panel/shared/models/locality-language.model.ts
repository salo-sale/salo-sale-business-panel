import {DeserializableInterface} from '../../../../shared/models/interfaces/deserializable.interface';
import {SerializableInterface} from '../../../../shared/models/interfaces/serializable.interface';
import {CityLanguageInterface} from './interfaces/city-language.interface';

export class LocalityLanguageModel implements
  CityLanguageInterface,
  DeserializableInterface<CityLanguageInterface>,
  SerializableInterface<CityLanguageInterface> {

  languageCode: string;
  name: string;
  localityId: number;
  id: number;

  deserialize(input: CityLanguageInterface): this {

    return Object.assign(this, input);

  }

  serialize(): CityLanguageInterface {

    return Object.assign({} as CityLanguageInterface, this);

  }

}
