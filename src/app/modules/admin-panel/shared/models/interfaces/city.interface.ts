import {CityLanguageInterface} from './city-language.interface';

export interface CityInterface {

  id: number;
  languages: CityLanguageInterface[];
  languagesStrings: string[];
  zoom: number;
  longitude: number;
  latitude: number;
  status: number;
  countryId: number;
  regionId: number;
  cityId: number;
  idList: number[];

}
