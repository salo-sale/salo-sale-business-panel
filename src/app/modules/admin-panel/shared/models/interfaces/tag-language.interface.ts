export interface TagLanguageInterface {

  id: number;
  languageCode: string;
  name: string;

}
