export interface DashboardInterface {

  userCount: number;
  companyCount: number;
  pointOfSaleCount: number;
  advertisingBannerCount: number;
  offerCount: number;
  contractCount: number;

}
