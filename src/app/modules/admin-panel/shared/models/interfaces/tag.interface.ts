import {TagLanguageInterface} from './tag-language.interface';

export interface TagInterface {

  tagId: number;
  id: number;
  languages: TagLanguageInterface[];
  icon: string;
  name?: string;
  status: number;

}
