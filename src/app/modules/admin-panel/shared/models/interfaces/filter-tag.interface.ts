export interface FilterTagInterface {

  page: number;
  count: number;
  orderBy: string;
  sort: string;
  status: number;

}
