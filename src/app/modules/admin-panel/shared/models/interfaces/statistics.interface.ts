export interface StatisticsInterface {

  id: number;
  type: string;
  value: number;
  status: number;
  created_at: string;
  updated_at: string;

}
