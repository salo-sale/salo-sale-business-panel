export interface FilterCityInterface {

  page: number;
  count: number;
  orderBy: string;
  sort: string;
  search: string;
  status: number;
  type: number;

}
