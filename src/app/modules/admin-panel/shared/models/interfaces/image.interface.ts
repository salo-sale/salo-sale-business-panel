export interface ImageInterface {

  fileName: string;
  host: string;
  requestScheme: string;
  path: string;

}
