import {DeserializableInterface} from '../../../../shared/models/interfaces/deserializable.interface';
import {SerializableInterface} from '../../../../shared/models/interfaces/serializable.interface';
import {StatisticsInterface} from './interfaces/statistics.interface';

export class StatisticsModel implements StatisticsInterface, DeserializableInterface<StatisticsInterface>, SerializableInterface<StatisticsInterface> {

  created_at: string;
  id: number;
  status: number;
  type: string;
  updated_at: string;
  value: number;

  deserialize(input: StatisticsInterface): this {

    return Object.assign(this, input);

  }

  serialize(): StatisticsInterface {

    return Object.assign({} as StatisticsInterface, this);

  }

}
