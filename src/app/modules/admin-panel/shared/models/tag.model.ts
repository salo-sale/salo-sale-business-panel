import {TagInterface} from './interfaces/tag.interface';
import {DeserializableInterface} from '../../../../shared/models/interfaces/deserializable.interface';
import {SerializableInterface} from '../../../../shared/models/interfaces/serializable.interface';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {TagLanguageModel} from './tag-language.model';
import {isNullOrUndefined} from "util";

export class TagModel implements TagInterface, DeserializableInterface<TagInterface>, SerializableInterface<TagInterface> {

  /**
   *
   * Declaration variables
   *
   */

  private formBuilder: FormGroup;

  icon: string;
  id: number;
  languages: TagLanguageModel[];
  status: number;
  tagId: number;

  _isNew: boolean;
  name: string;

  constructor() {

    this._isNew = true;
    this.initForm();

  }

  deserialize(input: TagInterface): this {

    this._isNew = false;

    const object = Object.assign(this, input);

    object.languages = object.languages.map((name) => new TagLanguageModel().deserialize(name));

    return object;

  }

  serialize(): TagInterface {

    const object = Object.assign({} as TagInterface, this);
    delete object._isNew;
    delete object.formBuilder;
    delete object.name;

    return object;

  }

  get formGroup(): FormGroup {

    return this.formBuilder;

  }

  get validForm(): boolean {

    if (this.formGroup.valid) {


      Object.keys(this.formGroup.value).forEach(key => {

        if (!isNullOrUndefined(this.formGroup.value[key])) {

          this[key] = this.formGroup.value[key];

        }

      });

      return true;

    } else {

      return false;

    }

  }

  public initForm(): void {

    // Todo get info about hoive many langauges

    let languages = [
      new FormGroup({

        languageCode: new FormControl('en', [Validators.required]),
        name: new FormControl('', [Validators.required]),

      }),
      new FormGroup({

        languageCode: new FormControl('ru', [Validators.required]),
        name: new FormControl('', [Validators.required]),

      }),
      new FormGroup({

        languageCode: new FormControl('uk', [Validators.required]),
        name: new FormControl('', [Validators.required]),

      })
    ];

    if (!this._isNew) {

      languages = [...this.languages.map((name) => {

        return new FormGroup({

          id: new FormControl(name.id, [Validators.required]),
          languageCode: new FormControl(name.languageCode, [Validators.required]),
          name: new FormControl(name.name, [Validators.required]),

        });

      })];

    }

    this.formBuilder = new FormBuilder().group({
      status: [
        this.status
      ],
      icon: [
        this.icon,
        [
          Validators.required,
        ]
      ],
      languages: new FormArray(languages, [Validators.required]),
    });

  }

}
