import { Injectable, PLATFORM_ID, Inject } from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../../../shared/services/auth.service';
import {RequestConfigurationModel} from '../../../../shared/models/request-configuration.model';
import {AgreementInterface} from '../models/interfaces/agreement.interface';
import {AgreementModel} from '../models/agreement.model';
import {LocalStorageTool} from '../../../../shared/tools/local-storage.tool';
import {LocalityAdminModel} from '../../../admin-panel/shared/models/locality-admin.model';

/**
 *
 */
@Injectable({
  providedIn: 'root'
})
export class AuthPanelService {

  public agreements: AgreementModel[];

  /**
   *
   * @param router
   * @param authService
   * @param platformId
   */
  constructor(
    public router: Router,
    public authService: AuthService,
    @Inject(PLATFORM_ID) private platformId
  ) {

    // this.agreements = LocalStorageTool.getAgreements();
    this.agreements = [];

  }

  async getAgreements(reset: boolean = false): Promise<AgreementModel[]> {

    return new Promise<AgreementModel[]>(async (resolve, reject) => {

      if (this.agreements.length === 0 || reset) {

        const requestConfiguration = new RequestConfigurationModel().deserialize({
          path: 'auth',
          url: 'agreements'
        });

        await this.authService.apiService.request(requestConfiguration).subscribe((agreements: AgreementInterface[]) => {

          console.log(agreements);

          this.agreements = [];

          this.agreements = agreements.map((agreement) => new AgreementModel().deserialize(agreement));

          resolve(this.agreements);

          // LocalStorageTool.setAgreements(this.agreements);

        });

      } else {

        resolve(this.agreements);

      }

    });

  }

}
