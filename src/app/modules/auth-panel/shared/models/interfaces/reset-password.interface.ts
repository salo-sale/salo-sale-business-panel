export interface ResetPasswordInterface {

  email: string;
  newPassword: string;
  newPasswordRepeat: string;
  code: string;

}
