export interface LoginInterface {

  email: string;
  password: string;

  showPassword: boolean;
  showPasswordRepeat: boolean;

}
