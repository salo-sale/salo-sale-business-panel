import {AgreementModel} from '../agreement.model';
import {AgreementInterface} from './agreement.interface';

export interface RegistrationInterface {

  // phone: string;
  agreements: AgreementInterface[];
  email: string;
  // username: string;
  password: string;
  passwordRepeat: string;
  invitationCode: string;
  // termsOfUse: boolean;

  showPassword: boolean;
  showPasswordRepeat: boolean;

}
