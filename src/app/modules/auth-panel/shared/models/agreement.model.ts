import {AgreementInterface} from './interfaces/agreement.interface';
import {DeserializableInterface} from '../../../../shared/models/interfaces/deserializable.interface';
import {SerializableInterface} from '../../../../shared/models/interfaces/serializable.interface';

export class AgreementModel implements AgreementInterface, DeserializableInterface<AgreementInterface>, SerializableInterface<AgreementInterface> {

  agreement: string;
  id: number;
  required: number;
  url: string;

  deserialize(input: AgreementInterface): this {

    return Object.assign(this, input);

  }

  serialize(): AgreementInterface {

    return Object.assign({} as AgreementInterface, this);

  }

}
