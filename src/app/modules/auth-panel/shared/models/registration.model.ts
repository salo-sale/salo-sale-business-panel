import {RegistrationInterface} from './interfaces/registration.interface';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {EMAIL_PATTERN} from '../../../../shared/tools/patterns.tool';
import {AgreementModel} from './agreement.model';
import {isNullOrUndefined} from 'util';
import {ValidationErrors} from '@angular/forms/src/directives/validators';

export class RegistrationModel implements RegistrationInterface {

  constructor() {

    // this.init();

  }

  get formGroup(): FormGroup {

    return this.formBuilder;

  }

  /**
   *
   * Declaration variables
   *
   */

  public agreements: AgreementModel[];

  public agreementsIds: number[];

  public email: string;
  public invitationCode: string;
  public password: string;
  public passwordRepeat: string;

  private formBuilder: FormGroup;

  public showPassword: boolean;
  public showPasswordRepeat: boolean;

  get valid(): boolean {

    if (this.formGroup.valid) {

      this.email = this.formGroup.get('email').value;
      this.invitationCode = this.formGroup.get('invitationCode').value;
      this.password = this.formGroup.get('password').value;
      this.passwordRepeat = this.formGroup.get('passwordRepeat').value;

      // this.passwordRepeat = this.formGroup.get('agreements').value;
      // this.termsOfUse = this.formGroup.get('termsOfUse').value;
      // console.log(this.formGroup.get('agreements').value);

      this.agreementsIds = [];

      (this.formGroup.get('agreements').value as Array<boolean>).forEach((value, key) => {

        if (value) {

          this.agreementsIds.push(this.agreements[key].id);

        }

      });

      return true;

    } else {

      return false;

    }

  }

  /**
   *
   * @param group
   *
   */
   static checkPasswords(group: FormGroup) {

    if (group.controls.passwordRepeat.value.toString().length > 5) {

      const isConfirmation = group.controls.password.value === group.controls.passwordRepeat.value;

      if (!isConfirmation) {
        group.controls.passwordRepeat.setErrors({MatchPassword: true});
      }

      return isConfirmation ? null : {MatchPassword: true};

    } else {

      return null;

    }

  }

  /**
   *
   * @param group
   *
   */
   static additionalValidators(group: FormGroup) {

     RegistrationModel.checkPasswords(group);

  }

  public init() {

    this.email = '';
    this.invitationCode = '';
    this.password = '';
    this.passwordRepeat = '';
    this.agreementsIds = [];
    // this.termsOfUse = null;

    this.showPassword = false;
    this.showPasswordRepeat = false;

    this.formBuilder = new FormBuilder().group({
      email: [
        this.email,
        [
          Validators.required,
          Validators.pattern(EMAIL_PATTERN)
        ]
      ],
      invitationCode: [
        this.invitationCode,
      ],
      password: [
        this.password,
        [
          Validators.required,
          Validators.minLength(6),
        ]
      ],
      passwordRepeat: [
        this.passwordRepeat,
        [
          Validators.required,
          Validators.minLength(6),
        ]
      ],
      agreements: new FormArray([...this.agreements.map((agreement) => {
        return new FormControl(null, agreement.required ? [Validators.requiredTrue] : null);
      })], [Validators.required]), // Validators not working
    }, { validator: RegistrationModel.additionalValidators });

  }

}
