import {ResetPasswordInterface} from './interfaces/reset-password.interface';
import {DeserializableInterface} from '../../../../shared/models/interfaces/deserializable.interface';
import {SerializableInterface} from '../../../../shared/models/interfaces/serializable.interface';

export class ResetPasswordModel
  implements
  ResetPasswordInterface,
  DeserializableInterface<ResetPasswordInterface>,
  SerializableInterface<ResetPasswordInterface> {

  code: string;
  email: string;
  newPassword: string;
  newPasswordRepeat: string;

  constructor() {

  }

  deserialize(input: ResetPasswordInterface): this {

    return Object.assign(this, input);

  }

  serialize(): ResetPasswordInterface {

    return Object.assign({} as ResetPasswordInterface, this);

  }

}
