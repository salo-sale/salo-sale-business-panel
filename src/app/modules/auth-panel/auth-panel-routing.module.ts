import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthPanelComponent} from './auth-panel.component';
import {LoginPageComponent} from './ui/pages/login-page/login-page.component';
import {RegistrationPageComponent} from './ui/pages/registration-page/registration-page.component';
import {ConfirmEmailPageComponent} from './ui/pages/confirm-email-page/confirm-email-page.component';

const routes: Routes = [{
  path: '',
  component: AuthPanelComponent,
  // pathMatch: 'full',
  children: [{
    path: '',
    component: LoginPageComponent
  }, {
    path: 'login',
    component: LoginPageComponent
  }, {
    path: 'registration',
    component: RegistrationPageComponent
  }, {
    path: 'confirm-email/:verificationToken',
    component: ConfirmEmailPageComponent
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AuthPanelRoutingModule { }
