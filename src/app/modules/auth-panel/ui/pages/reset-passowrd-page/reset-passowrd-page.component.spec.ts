import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResetPassowrdPageComponent } from './reset-passowrd-page.component';

describe('ResetPassowrdPageComponent', () => {
  let component: ResetPassowrdPageComponent;
  let fixture: ComponentFixture<ResetPassowrdPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResetPassowrdPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetPassowrdPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
