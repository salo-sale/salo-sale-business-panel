import {Component, forwardRef, Inject, OnInit} from '@angular/core';
import {LoginModel} from '../../../shared/models/login.model';
import {AuthPanelService} from '../../../shared/services/auth-panel.service';
import {Router} from '@angular/router';
import {AuthService} from '../../../../../shared/services/auth.service';

@Component({
  selector: 'app-client-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  public form: LoginModel;
  public showLoading: boolean;
  public showGoodStatus: boolean;

  constructor(
    public authService: AuthService
  ) {

    this.form = new LoginModel();

  }

  ngOnInit() {

    this.showLoading = false;
    this.showGoodStatus = false;

  }

  login() {

    this.showLoading = true;
    if (this.form.valid) {

      console.log(this.form);

      this.authService.login(this.form.email, this.form.password).then((result) => {

        console.log(result);

        this.showLoading = false;

        if (result) {

          this.showGoodStatus = true;

          setTimeout(() => {

            this.authService.redirect('/business');

          }, 2500);

        } else {

          this.showGoodStatus = false;

        }

      }).catch((error) => {

        console.log(error);
        this.showLoading = false;
        this.showGoodStatus = false;

      });

    }

  }

}
