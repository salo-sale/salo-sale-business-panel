import { Component, OnInit } from '@angular/core';
import {isNullOrUndefined} from "util";
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../../../../shared/services/auth.service';

@Component({
  selector: 'app-confirm-email-page',
  templateUrl: './confirm-email-page.component.html',
  styleUrls: ['./confirm-email-page.component.scss']
})
export class ConfirmEmailPageComponent implements OnInit {
  public result: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    public authService: AuthService
  ) {

    this.result = null;

  }

  ngOnInit() {

    this.activatedRoute.parent.firstChild.params.subscribe(async (params) => {

      console.log(params);
      if (!isNullOrUndefined(params.verificationToken)) {

        this.authService.sendConfirmEmail(params.verificationToken).then((result) => {

          this.result = result;

        });

      }

    });

  }

}
