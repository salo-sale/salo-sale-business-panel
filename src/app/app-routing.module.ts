import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthService} from './shared/services/auth.service';

export const routes: Routes = [{
  path: 'business',
  loadChildren: './modules/admin-panel/admin-panel.module#AdminPanelModule',
  canActivate: [AuthService],
}, {
  path: '',
  loadChildren: './modules/auth-panel/auth-panel.module#AuthPanelModule',
  canActivate: [AuthService],
}, {
  path: '**',
  redirectTo: 'auth'
}];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      initialNavigation: 'enabled',
      scrollPositionRestoration: 'top',
      useHash: true
    }),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
