import {LanguageModel} from '../models/language.model';
import {CityModel} from '../models/city.model';
import {isNullOrUndefined} from 'util';
import {AgreementModel} from '../../modules/auth-panel/shared/models/agreement.model';
import {TagModel} from '../../modules/admin-panel/shared/models/tag.model';
import {FilterTagModel} from '../../modules/admin-panel/shared/models/filter-tag.model';
import {CompanyModel} from '../../modules/admin-panel/modules/admin-panel-company/shared/models/company.model';
import {FilterModel} from '../../modules/admin-panel/modules/admin-panel-company/shared/models/filter.model';
import {OfferModel} from '../../modules/admin-panel/modules/admin-panel-offer/shared/models/offer.model';
import {ListOfTagsModel} from '../models/list-of-tags.model';

export class LocalStorageTool {

  // ExpiresIn

  static getExpiresIn(): number {

    return JSON.parse(localStorage.getItem('expiresIn'));

  }

  static setExpiresIn(expiresIn: number) {

    localStorage.setItem('expiresIn', JSON.stringify(expiresIn));

  }

  // Languages

  static getLanguages(): LanguageModel[] {

    const languages = JSON.parse(localStorage.getItem('languages'));
    return isNullOrUndefined(languages) ? [] : (languages).map((language) => new LanguageModel().deserialize(language));

  }

  static setLanguages(languages: LanguageModel[]) {

    localStorage.setItem('languages', JSON.stringify(languages.map((language) => language.serialize())));

  }

  // Cities

  static getCities(): CityModel[] {

    const cities = JSON.parse(localStorage.getItem('cities'));
    return isNullOrUndefined(cities) ? [] : (cities).map((city) => new CityModel().deserialize(city));

  }

  static setCities(cities: CityModel[]) {

    localStorage.setItem('cities', JSON.stringify(cities.map((city) => city.serialize())));

  }

  // Cities

  static getListsOfTags(): ListOfTagsModel[] {

    const listsOfTags = JSON.parse(localStorage.getItem('listsOfTags'));
    return isNullOrUndefined(listsOfTags) ? [] : (listsOfTags).map((list) => new ListOfTagsModel().deserialize(list));

  }

  static setListsOfTags(listsOfTags: ListOfTagsModel[]) {

    localStorage.setItem('listsOfTags', JSON.stringify(listsOfTags.map((list) => list.serialize())));

  }


  // Agreements

  static getAgreements(): AgreementModel[] {

    const agreements = JSON.parse(localStorage.getItem('agreements'));
    return isNullOrUndefined(agreements) ? [] : (agreements).map((agreement) => new AgreementModel().deserialize(agreement));

  }

  static setAgreements(agreements: AgreementModel[]) {

    localStorage.setItem('agreements', JSON.stringify(agreements.map((agreement) => agreement.serialize())));

  }

  // Tags

  static getTags(): TagModel[] {

    const tags = JSON.parse(localStorage.getItem('tags'));
    return isNullOrUndefined(tags) ? [] : (tags).map((tag) => new TagModel().deserialize(tag));

  }

  static setTags(tags: TagModel[]) {

    localStorage.setItem('tags', JSON.stringify(tags.map((tag) => tag.serialize())));

  }

  // FilterTag

  static getFilterTag(): FilterTagModel {

    return new FilterTagModel().deserialize(JSON.parse(localStorage.getItem('filterTag')));

  }

  static setFilterTag(filterTag: FilterTagModel) {

    localStorage.setItem('filterTag', JSON.stringify(filterTag.serialize()));

  }

  // Companies

  static getCompanies(): CompanyModel[] {

    const companies = JSON.parse(localStorage.getItem('companies'));
    return isNullOrUndefined(companies) ? [] : (companies).map((company) => new CompanyModel().deserialize(company));

  }

  static setCompanies(companies: CompanyModel[]) {

    localStorage.setItem('companies', JSON.stringify(companies.map((company) => company.serialize())));

  }

  // FilterCompany

  static getFilterCompany(): FilterModel {

    return new FilterModel().deserialize(JSON.parse(localStorage.getItem('filterCompany')));

  }

  static setFilterCompany(filterCompany: FilterModel) {

    localStorage.setItem('filterCompany', JSON.stringify(filterCompany.serialize()));

  }

  // Offers

  static getOffers(): OfferModel[] {

    const offers = JSON.parse(localStorage.getItem('offers'));
    return isNullOrUndefined(offers) ? [] : (offers).map((offer) => new OfferModel().deserialize(offer));

  }

  static setOffers(offers: OfferModel[]) {

    localStorage.setItem('offers', JSON.stringify(offers.map((offer) => offer.serialize())));

  }

  // FilterTag
  // TODO change filterModel to FilterOfferModel

  static getFilterOffer(): FilterModel {

    return new FilterModel().deserialize(JSON.parse(localStorage.getItem('filterOffer')));

  }

  static setFilterOffer(filterCompany: FilterModel) {

    localStorage.setItem('filterOffer', JSON.stringify(filterCompany.serialize()));

  }

  // Cities admin

  // static getAdminCities(): City[] {
  //
  //   const offers = JSON.parse(localStorage.getItem('adminCities'));
  //   return isNullOrUndefined(offers) ? [] : (offers).map((offer) => new OfferModel().deserialize(offer));
  //
  // }
  //
  // static setAdminCities(offers: OfferModel[]) {
  //
  //   localStorage.setItem('adminCities', JSON.stringify(offers.map((offer) => offer.serialize())));
  //
  // }

  // Filter cities admin

  // static getAdminFilterCity(): FilterOfferModel {
  //
  //   return new FilterOfferModel().deserialize(JSON.parse(localStorage.getItem('filterAdminCity')));
  //
  // }
  //
  // static setAdminFilterCity(filterCompany: FilterOfferModel) {
  //
  //   localStorage.setItem('filterAdminCity', JSON.stringify(filterCompany.serialize()));
  //
  // }

}
