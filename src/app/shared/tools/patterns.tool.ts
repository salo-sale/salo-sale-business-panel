export const EMAIL_PATTERN = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
export const USERNAME_PATTERN = /^[a-zA-Z0-9_]{1,32}$/;
