export enum UserPurchasedStatusEnum {

    'Заблокована',
    'Неактивна',
    'Куплено',
    'Використано'

  }
