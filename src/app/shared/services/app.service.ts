import { Injectable } from '@angular/core';
import {AuthService} from './auth.service';
import {LanguageModel} from '../models/language.model';
import {CityModel} from '../models/city.model';
import {RequestConfigurationModel} from '../models/request-configuration.model';
import {CityInterface} from '../models/interfaces/city.interface';
import {LanguageInterface} from '../models/interfaces/language.interface';
import {LocalStorageTool} from '../tools/local-storage.tool';
import {ListOfTagsModel} from '../models/list-of-tags.model';
import {ListOfTagsInterfaces} from '../models/interfaces/list-of-tags.interfaces';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  private initialized: boolean;

  public languages: LanguageModel[];
  public cities: CityModel[];
  listsOfTags: ListOfTagsModel[];

  constructor(
    public authService: AuthService
  ) {

    this.initialized = false;
    this.cities = LocalStorageTool.getCities();
    this.languages = LocalStorageTool.getLanguages();
    this.listsOfTags = LocalStorageTool.getListsOfTags();

  }

  init() {

    if (!this.initialized) {

      this.initialized = true;

      this.authService.init();

      this.getLanguages();
      this.getCities();
      this.getListsOfTags();

    }

  }

  restInit() {

    if (this.initialized) {

      this.initialized = false;
      this.init();

    }

  }

  /**
   *
   * @param reset
   */
  getListsOfTags(reset: boolean = false) {

    if (this.listsOfTags.length === 0 || reset) {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        url: 'listsOfTags'
      });

      this.authService.apiService.request(requestConfiguration).subscribe((lists: ListOfTagsInterfaces[]) => {

        console.log(lists);

        this.listsOfTags = [];

        this.listsOfTags = lists.map((list) => new ListOfTagsModel().deserialize(list));

        LocalStorageTool.setListsOfTags(this.listsOfTags);

      });

    } else {

      return this.listsOfTags;

    }

  }

  /**
   *
   * @param reset
   */
  getCities(reset: boolean = false) {

    if (this.cities.length === 0 || reset) {

      this.cities = [];

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        url: 'cities'
      });

      this.authService.apiService.request(requestConfiguration).subscribe((cities: CityInterface[]) => {

        console.log(cities);

        this.cities = cities.map((city) => new CityModel().deserialize(city));

        LocalStorageTool.setCities(this.cities);

      });

    } else {

      return this.cities;

    }

  }

  /**
   *
   * @param reset
   */
  getLanguages(reset: boolean = false) {

    if (this.languages.length === 0 || reset) {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        url: 'languages'
      });

      this.authService.apiService.request(requestConfiguration).subscribe((languages: LanguageInterface[]) => {

        console.log(languages);

        this.languages = [];

        this.languages = languages.map((language) => new LanguageModel().deserialize(language));

        LocalStorageTool.setLanguages(this.languages);

      });

    } else {

      return this.languages;

    }

  }

}
