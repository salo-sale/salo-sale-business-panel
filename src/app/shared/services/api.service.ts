import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {UserModel} from '../models/user.model';
import {isNullOrUndefined, isObject} from 'util';
import {RequestConfigurationModel} from '../models/request-configuration.model';
import {NotificationService} from '../../modules/admin-panel/shared/modules/notification/shared/services/notification.service';
import {NotificationStyle} from '../../modules/admin-panel/shared/modules/notification/shared/enums/notification-style.enum';
import {throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  public user: UserModel;
  private initialized: boolean;
  public languageCode: string;

  constructor(
    public http: HttpClient,
    public notificationService: NotificationService,
  ) {

    this.initialized = false;

  }



  public handleError(error: HttpErrorResponse) {

    console.log(error);

    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);

    } else {

      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);

    }

    if (isObject(error.error)) {

      if (error.status === 404 || error.status === 500) {

        this.notificationService.addToStream({
          body: error.message,
          show: true,
          style: NotificationStyle.ERROR,
          title: 'Помилка'
        });

      } else {

        const errorsKeys = Object.keys(error.error);

        if (errorsKeys.length > 0) {

          errorsKeys.forEach((keyError) => {

            this.notificationService.addToStream({
              body: error.error[keyError],
              show: true,
              style: NotificationStyle.ERROR,
              title: 'Помилка'
            });

          });

        } else {

          this.notificationService.addToStream({
            body: 'Повідомити адміністратора про помилку. <br /> Статус помилки: <strong>' + error.status + '</strong>. <br /> E-mail: <strong>feedback@salo-sale.com</strong>',
            show: true,
            useTimer: false,
            style: NotificationStyle.ERROR,
            title: 'Помилка'
          });

        }

      }

    }

    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');

  }

  init(user: UserModel) {

    if (!this.initialized) {

      this.user = user;
      this.languageCode = environment.defaultDate.languageCode;
      this.initialized = false;

    }

  }

  restInit() {

    if (this.initialized) {

      this.initialized = false;

    }

  }

  private buildHeaders(useContentType = false, methodOverwrite = '', isLogout: boolean = false) {

    const headers = {};

    if (useContentType) {

      headers['Content-Type'] = 'application/x-www-form-urlencoded';

    }

    if (methodOverwrite !== '') {

      headers['X-HTTP-Method-Override'] = methodOverwrite;

    }

    headers['Accept-Language'] = this.languageCode;
    headers['client_id'] = environment.api.client_id;
    headers['client_secret'] = environment.api.client_secret;

    if (isNullOrUndefined(this.user) || isNullOrUndefined(this.user.accessToken) || this.user.status === 1) {

      headers['device_uuid'] = this.user.deviceUUID;

    } else {

      if (isLogout) {

        headers['device_uuid'] = this.user.deviceUUID;

      }

    }

    if (!isNullOrUndefined(this.user)) {

      headers['Authorization'] = 'Bearer ' + this.user.accessToken;

    }

    // console.log(headers);

    return headers;

  }

  private getOptions(useContentType = false, methodOverwrite = '', isLogout: boolean = false): object {

    const header = new HttpHeaders(JSON.parse(JSON.stringify(this.buildHeaders(useContentType, methodOverwrite, isLogout))));

    // console.log(header);

    return {
      headers: header
    };

  }

  /**
   *
   *      useContentType: false,
   *      type: null, // if null well be get method
   *      body: null
   *      }
   * @param requestConfiguration
   * @param isLogout
   */
  public request(requestConfiguration: RequestConfigurationModel, isLogout: boolean = false) {

    let url = environment.api.paths[requestConfiguration.path][requestConfiguration.url];
    const params = requestConfiguration.params;

    if (requestConfiguration.useUrlData) {

      url += requestConfiguration.subUrl;

    }

    let request = null;

    if (params == null) {

      request = this.http.get(url, this.getOptions());

    }

    if (isNullOrUndefined(request)) {

      let options = params.options != null ? params.options : this.getOptions(params.useContentType);

      switch (params.type) {

        case 'post':

          request = this.http.post(url, params.body, options);

          break;

        case 'put':

          options = params.options != null ? params.options : this.getOptions(params.useContentType, 'PUT');

          request = this.http.put(url, params.body, options);

          break;

        case 'delete':

          options = params.options != null ? params.options : this.getOptions(params.useContentType, 'DELETE', isLogout);
          request = this.http.delete(url, options);

          break;

        default:

          request = this.http.get(url, {
            headers: this.getOptions()['headers'],
            params: options
          });


      }

    }

    return request.pipe(catchError(this.handleError.bind(this)));

  }

  /**
   *
   * @param url
   */
  public uploadImageViaUrl(url: string): Promise<string> {

    return new Promise<string>(async resolve => {

      const options = { options: { responseType: 'blob' } };
      options['headers'] = this.getOptions()['headers'];
      //
      // await this.request(url, options, '', false).subscribe((blob) => {
      //
      //   const reader = new FileReader();
      //   reader.readAsDataURL(<Blob>blob);
      //   reader.onloadend = function() {
      //
      //     const base64data = reader.result.toString().replace('text/html', 'image');
      //
      //     resolve(<string>base64data);
      //
      //   };
      //
      // });

    });

  }


}
