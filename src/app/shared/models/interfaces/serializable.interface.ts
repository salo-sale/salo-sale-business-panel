export interface SerializableInterface<T> {
  serializeVar?: T;
  serialize(): T;
}
