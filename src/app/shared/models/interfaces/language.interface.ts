export interface LanguageInterface {

  code: string;
  name: string;
  country: string;

}
