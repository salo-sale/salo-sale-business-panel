export interface UserInterface {

  id: number;
  login: string;
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  roles: string[];
  status: number;
  gender: number;

  accessToken: string;
  expiresIn: string;
  refreshToken: string;

}
