export interface DeserializableInterface<T> {
  deserialize(input: T): this;
}
