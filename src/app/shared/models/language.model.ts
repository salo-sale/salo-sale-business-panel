import {LanguageInterface} from './interfaces/language.interface';
import {DeserializableInterface} from './interfaces/deserializable.interface';
import {SerializableInterface} from './interfaces/serializable.interface';

export class LanguageModel implements LanguageInterface, DeserializableInterface<LanguageInterface>, SerializableInterface<LanguageInterface>{

  code: string;
  country: string;
  name: string;

  deserialize(input: LanguageInterface): this {

    return Object.assign(this, input);

  }

  serialize(): LanguageInterface {

    return Object.assign({} as LanguageInterface, this);

  }

}
