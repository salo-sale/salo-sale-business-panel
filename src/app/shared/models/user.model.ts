import {UserInterface} from './interfaces/user.interface';
import {DeserializableInterface} from './interfaces/deserializable.interface';
import {SerializableInterface} from './interfaces/serializable.interface';
import {isNullOrUndefined} from 'util';
import {DeviceUUID} from '../../../../node_modules/device-uuid/lib/device-uuid';
import {CompanyModel} from "../../modules/admin-panel/modules/admin-panel-company/shared/models/company.model";
import {CompanyInterface} from "../../modules/admin-panel/modules/admin-panel-company/shared/models/interfaces/company.interface";
import {environment} from '../../../environments/environment';

export class UserModel implements UserInterface, DeserializableInterface<UserInterface>, SerializableInterface<UserInterface> {

  constructor() {

    this.deviceUUID = new DeviceUUID().get();

  }

  id: number;
  email: string;
  firstName: string;
  lastName: string;
  status: number;

  roles: string[];
  accessToken: string;
  expiresIn: string;
  refreshToken: string;

  deviceUUID: string;

  selectedCompany: CompanyModel;
  companies: CompanyModel[];

  /**
   * return Object
   */
  static loadFromLocalStorage(): UserModel | null {

    const user = JSON.parse(localStorage.getItem('salo-sale-business-panel-user-' + (environment.production ? 'prod' : 'dev')));
    // console.log(user);

    if (isNullOrUndefined(user)) {

      return null;

    } else {

      return new UserModel().deserialize(user);

    }

  }

  deserialize(input: UserInterface): this {

    const object = Object.assign(this, input);

    if (!isNullOrUndefined(object.selectedCompany)) {
      object.selectedCompany = new CompanyModel().deserialize(object.selectedCompany);
    }
    if (!isNullOrUndefined(object.companies)) {
      object.companies = object.companies.map((c) => new CompanyModel().deserialize(c));
    }

    return object;

  }

  serialize(): UserInterface {

    const object = Object.assign({} as UserInterface, this);

    if (!isNullOrUndefined(object.selectedCompany)) {
      object.selectedCompany = (object.selectedCompany as CompanyModel).serialize() as CompanyInterface & CompanyModel;
    }

    if (!isNullOrUndefined(object.companies)) {
      object.companies = object.companies.map((c) => (c as CompanyModel).serialize() as CompanyInterface & CompanyModel);
    }

    return object;

  }

  /**
   * return boolean
   */
  saveToLocalStorage(): boolean {

    localStorage.setItem('salo-sale-business-panel-user-' + (environment.production ? 'prod' : 'dev'), JSON.stringify(this.serialize()));
    return true;

  }

  accessTokenIsActive(): boolean {

    const date = this.expiresIn.split(' ')[0];

    if (!isNullOrUndefined(this.expiresIn)) {

      return (new Date(date).getTime()) >= Date.now();

    }

    return false;

  }

  checkSelectedCompany() {

    if (isNullOrUndefined(this.selectedCompany) && !isNullOrUndefined(this.companies) && this.companies.length > 0) {

      this.selectedCompany = this.companies[0];

    }

  }

  gender: number;
  login: string;
  phone: string;

}
