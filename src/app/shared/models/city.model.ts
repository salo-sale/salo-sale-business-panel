import {CityInterface} from './interfaces/city.interface';
import {DeserializableInterface} from './interfaces/deserializable.interface';
import {SerializableInterface} from './interfaces/serializable.interface';

export class CityModel implements CityInterface, DeserializableInterface<CityInterface>, SerializableInterface<CityInterface> {

  id: number;
  name: string;
  parentId: number;

  deserialize(input: CityInterface): this {

    return Object.assign(this, input);

  }

  serialize(): CityInterface {

    return Object.assign({} as CityInterface, this);

  }

}
