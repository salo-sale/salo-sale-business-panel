import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import { AppComponent } from './app.component';
import {ApiService} from './shared/services/api.service';
import {AuthService} from './shared/services/auth.service';
import {AppService} from './shared/services/app.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NotificationModule} from './modules/admin-panel/shared/modules/notification/notification.module';
import {AngularEditorModule} from "@kolkov/angular-editor";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    AppRoutingModule,
    AngularEditorModule,
    BrowserModule,
    NgbModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    NotificationModule

  ],
  providers: [
    ApiService,
    AuthService,
    AppService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {

  constructor(
    public appService: AppService
  ) {

    appService.init();

  }

}
