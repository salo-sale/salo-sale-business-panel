# REST API WITH YII2

## MODELS

## ENDPOINTS

## FUNCTIONS
### Auth
- [x] Registration
- [ ] Reset password
- [ ] Confirm email

### Company panel

- [ ] **Company page** [View, Update]

    - [ ] **PointsOfSale** [Create, Update, Delete, View]
    
        - [ ] **UserRolePointOfSale**
    
    - [ ] **Offers**
    - [ ] **Followers**

### Admin Panel

- [x] **Tags** [Create, -Delete, Update]
- [x] **ListOfTags** [Find, Create]

    - [x] **List** [AddNewTag, Update, DeleteTag, Delete]

- [x] **Companies** [Find, Create]
   
    - [x] **PointsOfSale** [Update, -Delete, View]
    - [x] **Offers** [Update, -Delete, View]
    - [x] **Contacts** [Update, -Delete, View]
    - [x] **Contracts** [Update, -Delete, View]

- [x] **PointsOfSale** [Find, Create]

    - [x] **PointOfSale** [Update, -Delete, View]

- [x] **Offers** [Find, Create]

    - [x] **Offer** [Update, -Delete, View]
    
        - [ ] **Offer image by language**
    
        - [x] **PointsOfSale** [Update, Delete, View]
        - [ ] **ValidityOffer** [Update, Delete, View]
        - [x] **CodesOffer** [-Update, -Delete, List]
        - [ ] **LinksOffer** [Update, Delete, View]
    
- [x] **Dashboard**

- [x] **Users** [Find]

    - [ ] **User** [View, Blocked]

- [x] **Localities** [Create, Update, Delete]
- [ ] **Notifications** [Find, Create]

    - [ ] **Notification** [View, Delete, Update]

NEED REWRITE!
- [ ] **AdvertisingBanners** [Find, Create]

    - [ ] **AdvertisingBanner** [View, Delete, Update]
